# -*- coding: utf-8 -*-
"""Utilities for the molz package."""

import re

import healpy as hp
import numpy as np
from scipy.spatial.transform import Rotation

from .constants import au2aa
from .elements import element, element_mass


def xyz2cart(s: str) -> list[list[float]]:
    """Convert string in XYZ molecular format to Cartesian coordinates.

    Args:
        s (str): Input coordinate string

    Returns:
        list[list[float]]: Cartesian coordinate quadruples

    """

    el, x, y, z = parse_xyz(s)
    return transpose(el, x, y, z)


def cart2xyz(cart: list[list[float]]) -> str:
    """Convert Cartesian coordinates to string in XYZ molecular format.

    Args:
        cart (list[list[float]]): Cartesian coordinates

    Returns:
        str: Coordinates as string in XYZ format (in Angstrom)

    """

    el, x, y, z = transpose(*cart)
    return format_xyz(el, x, y, z)


def parse_xyz(s: str) -> tuple:
    """Parse XYZ format and return element numbers and Cartesian coordinates.

    Args:
        s (str): Input coordinate string

    Returns:
        tuple: Element, X, Y, Z arrays

    """

    # Create regexes
    re_atom = re.compile(r'^\s*[a-zA-z]+')
    re_num = re.compile(r'[-]?\d+\.\d+')

    # Skip the first two lines
    _, _, atoms = s.split('\n', 2)

    el = []
    x = []
    y = []
    z = []

    # Iterate over atoms
    for line in atoms.splitlines():
        if not line.strip():
            continue
        m = re_atom.match(line)
        if m:
            atom = m.group(0).strip()
            el.append(element(atom))
            atx, aty, atz = [float(number) for number in re_num.findall(line)]
            x.append(atx)
            y.append(aty)
            z.append(atz)

    # Convert lists to numpy arrays
    el = np.array(el, dtype=int)
    x = np.array(x, dtype=float)
    y = np.array(y, dtype=float)
    z = np.array(z, dtype=float)

    # Convert lengths from Angstrom to au
    x = x / au2aa
    y = y / au2aa
    z = z / au2aa

    return el, x, y, z


def format_xyz(el: list[int], x: list[float], y: list[float], z: list[float]) -> str:
    """Write coordinates in XYZ format to a string.

    Args:
        el (list[int]): Element number array
        x (list[float]): X coordinates
        y (list[float]): Y coordinates
        z (list[float]): Z coordinates

    Returns:
        str: String in XYZ format.

    """

    # Convert to arrays
    if type(el) is not np.ndarray:
        el = np.array(el)

    if type(x) is not np.ndarray:
        x = np.array(x)

    if type(y) is not np.ndarray:
        y = np.array(y)

    if type(z) is not np.ndarray:
        z = np.array(z)

    if not (el.size == x.size == y.size == z.size):
        raise ValueError(f'Size mismatch in format_xyz: len(x)={x.size}, len(y)={y.size}, len(z)={z.size}')

    # Number of atoms
    s = '%d\n\n' % el.size

    # Convert lengths from au to Angstrom
    x = x * au2aa
    y = y * au2aa
    z = z * au2aa

    # Write atom symbols and coordinates
    for atom, atx, aty, atz in zip(el, x, y, z):
        s += ('%-2s %14.5f %14.5f %14.5f\n' % (element(int(atom)), atx, aty, atz))

    return s


def inert_frame(el: list[int], x: list[float], y: list[float], z: list[float]) -> tuple:
    """Transform to molecule-fixed reference frame.

    Args:
        el (list[int]): Element number array
        x (list[float]): X coordinates
        y (list[float]): Y coordinates
        z (list[float]): Z coordinates

    Returns:
        tuple: Cartesian coordinates in molecule-fixed reference frame (in au)

    """

    # Convert to arrays
    if x is not np.ndarray:
        x = np.array(x)

    if y is not np.ndarray:
        y = np.array(y)

    if z is not np.ndarray:
        z = np.array(z)

    # Stack Cartesian coordinate arrays
    cart = np.vstack([x, y, z]).T

    # Determine center of mass
    mass = np.array([element_mass(int(atom)) for atom in el])
    com = np.sum(mass[:, None] * cart, axis=0) / np.sum(mass)

    # Construct ellipsoid of inertia
    tmp = np.sqrt(mass[:, None]) * (cart - com[None, :])
    xx = np.dot(tmp[:, 0], tmp[:, 0])
    yy = np.dot(tmp[:, 1], tmp[:, 1])
    zz = np.dot(tmp[:, 2], tmp[:, 2])
    xy = np.dot(tmp[:, 0], tmp[:, 1])
    xz = np.dot(tmp[:, 0], tmp[:, 2])
    yz = np.dot(tmp[:, 1], tmp[:, 2])
    ellipsoid = np.array([[yy + zz, -xy, -xz],
                          [-xy, xx + zz, -yz],
                          [-xz, -yz, xx + yy]])

    # Diagonalize ellipsoid of inertia
    eigvals, eigvecs = np.linalg.eigh(ellipsoid)

    # Sort eigenvectors in ascending order (z has highest eigenvalue)
    ind = np.argsort(eigvals)
    eigvals = eigvals[ind]
    eigvecs = eigvecs[:, ind]

    # Ensure that the largest element of each eigenvector is positive
    # Not considering degeneracies
    ind = np.argmax(np.abs(eigvecs[:, 0]))
    if eigvecs[ind, 0] < 0.:
        eigvecs[:, 0] *= -1.0

    ind = np.argmax(np.abs(eigvecs[:, 1]))
    if eigvecs[ind, 1] < 0.:
        eigvecs[:, 1] *= -1.0

    ind = np.argmax(np.abs(eigvecs[:, 2]))
    if eigvecs[ind, 2] < 0.:
        eigvecs[:, 2] *= -1.0

    # Ensure right-handed coordinate system
    if np.dot(np.cross(eigvecs[:, 0], eigvecs[:, 1]), eigvecs[:, 2]) < 0.:
        eigvecs[:, 0] *= -1.

    # Rotate molecule into inertial coordinate system
    inert = np.dot(cart - com[None, :], eigvecs)

    # Deconstruct inert array
    x = inert[:, 0]
    y = inert[:, 1]
    z = inert[:, 2]

    return x, y, z


def transpose(*columns: list[float]) -> list[list[float]]:
    """Transpose input arrays.

    Args:
        *columns: list: Value arrays

    Returns:
        tuple: Transposed values

    """
    return [list(row) for row in zip(*columns)]


def cart2sph(x: list[float], y: list[float], z: list[float]) -> tuple:
    """Convert Cartesian to spherical polar coordinates.

    Args:
        x: list[float]: X coordinate array (in au)
        y: list[float]: Y coordinate array (in au)
        z: list[float]: Z coordinate array (in au)

    Returns:
        tuple: Radial, polar, azimuthal coordinates

    """

    if type(x) is not np.ndarray:
        x = np.array(x)
    if type(y) is not np.ndarray:
        y = np.array(y)
    if type(z) is not np.ndarray:
        z = np.array(z)

    if not x.size == y.size == z.size:
        raise ValueError(f'Size mismatch in cart2sph: len(x)={x.size}, len(y)={y.size}, len(z)={z.size}')

    xy = x ** 2 + y ** 2
    rad = np.sqrt(xy + z ** 2)
    theta = np.arctan2(np.sqrt(xy), z)
    phi = np.arctan2(y, x)

    return rad, theta, phi


def sph2cart(rad: list[float], theta: list[float], phi: list[float]) -> tuple:
    """Convert spherical polar to Cartesian coordinates.

    Args:
        rad (list[float]): Radial coordinate array (in au)
        theta (list[float]): Polar angle coordinate array (in rad)
        phi (list[float]): Azimuthal angle coordinate array (in rad)

    Returns:
        list[list[float]]: X, Y, Z coordinates

    """

    if type(rad) is not np.ndarray:
        rad = np.array(rad)
    if type(theta) is not np.ndarray:
        theta = np.array(theta)
    if type(phi) is not np.ndarray:
        phi = np.array(phi)

    if not rad.size == theta.size == phi.size:
        raise ValueError(
            f'Size mismatch in sph2cart: len(rad)={rad.size}, len(theta)={theta.size}, len(phi)={phi.size}')

    x = rad * np.sin(theta) * np.cos(phi)
    y = rad * np.sin(theta) * np.sin(phi)
    z = rad * np.cos(theta)

    return x, y, z


def get_min_max(coord: list[list[float]]):
    """Compute minimum and maximum values of 3D coordinate along each direction.

    Args:
        coord (list[list[float]]): 3D coordinates

    Returns:
        tuple: Minimum and maximum values

    """

    x, y, z = transpose(*coord)

    return np.amin(x), np.amax(x), np.amin(y), np.amax(y), np.amin(z), np.amax(z)


def transform_3d(coord: list[list[float]], coord_type: str,
                 input_type: str, to_inertial_frame: bool = False) -> list[list[float]]:
    """Convert between Cartesian and spherical coordinates with element.

    Args:
        coord (list[list[float]]): Input coordinates
        coord_type (str): Output coordinate type
        input_type (str): Input coordinate type
        to_inertial_frame (bool): Transform into molecule-fixed coordinate frame

    Returns:
        list[list[float]]: Transformed coordinates

    """

    if coord_type == 'cartesian':

        if input_type == 'cartesian':

            if to_inertial_frame:
                el, x, y, z = transpose(*coord)
                x, y, z = inert_frame(el, x, y, z)
                output = transpose(el, x, y, z)
            else:
                output = coord

        elif input_type == 'spherical':
            el, rad, theta, phi = transpose(*coord)
            x, y, z = sph2cart(rad, theta, phi)
            if to_inertial_frame:
                x, y, z = inert_frame(el, x, y, z)
            output = transpose(el, x, y, z)

        else:
            raise ValueError(f'Unknown coordinate input type {input_type}')

    elif coord_type == 'spherical':

        if input_type == 'cartesian':

            el, x, y, z = transpose(*coord)
            if to_inertial_frame:
                x, y, z = inert_frame(el, x, y, z)
            rad, theta, phi = cart2sph(x, y, z)
            output = transpose(el, rad, theta, phi)

        elif input_type == 'spherical':

            if to_inertial_frame:
                el, rad, theta, phi = transpose(*coord)
                x, y, z = sph2cart(rad, theta, phi)
                x, y, z = inert_frame(el, x, y, z)
                rad, theta, phi = cart2sph(x, y, z)
                output = transpose(el, rad, theta, phi)
            else:
                output = coord

        else:
            raise ValueError(f'Unknown coordinate input type {input_type}')

    else:
        raise ValueError(f'Unknown coordinate type {coord_type}')

    return output


def combine_elements(elements: list[float], coord: list[list[float]]) -> list[list[float]]:
    """Combine coordinates and element identifiers to 3D coordinate arrays.

    Args:
        elements (list[float]): Element identifiers
        coord (list[list[float]]): Input 3D coordinates

    Returns:
        list[list[float]]: 3D coordinates including element identifiers

    """

    coord1, coord2, coord3 = transpose(*coord)
    return transpose(elements, coord1, coord2, coord3)


def split_elements(coord: list[list[float]]) -> tuple:
    """Remove element identifiers (index 0) from 3D coordinate arrays.

    Args:
        coord (list[list[float]]): Input 3D coordinates

    Returns:
        tuple: Element identifiers, 3D coordinates

    """

    el, coord1, coord2, coord3 = transpose(*coord)
    return el, transpose(coord1, coord2, coord3)


def discretize(coord: float, width: float, offset: float, n_bins: int) -> int:
    """Discretize continuous coordinate in [-offset, -offset + width] into bins.
    Coordinates outside discretization box are truncated to outermost bins.

    Args:
        coord (float): Coordinate value
        width (float): Width of discretization box
        offset (float): Offset of discretization box
        n_bins (int): Number of bins

    Returns:
        int: Bin index

    """

    res = width / n_bins
    return max(0, min(int((coord + offset) / res), n_bins - 1))


def undiscretize(index: int, width: float, offset: float, n_bins: int) -> float:
    """Convert bin index in [0, n_bins - 1] to continuous coordinate.

    Args:
        index (int): Bin index
        width (float): Width of discretization box
        offset (float): Offset of discretization box
        n_bins (int): Number of bins

    Returns:
        float: Coordinate value

    """

    res = width / n_bins
    return (float(index) + 0.5) * res - offset


def interleave(c: list[int], n_bits: int, n_dims: int) -> int:
    """Interleave bits in n_bits long, n_dims dimensional representation of integer array c.

    Args:
        c (list[int]): Input coordinates
        n_bits (int): Bit length
        n_dims (int): Coordinate length

    Returns:
        int: Output coordinate with bits interleaved
    """

    # Create binary string representation
    bin_str = ''.join(np.binary_repr(x, width=n_bits) for x in c)

    # Interleave the bin numbers using string operations
    zbin_str = ''.join(np.array(list(bin_str)).reshape(n_dims, n_bits).T.flat)

    # Convert the bin number to integer
    return int(zbin_str, 2)


def deinterleave(z: int, n_bits: int, n_dims: int) -> list[int]:
    """Deinterleave bits in one-dimensional coordinate representation.

    Args:
        z (int): Input coordinate
        n_bits (int): Bit length
        n_dims (int): Coordinate length

    Returns:
        list[int]: Output coordinates with bits deinterleaved
    """

    # Convert to binary string representation
    zbin_str = np.binary_repr(z, width=(n_bits * n_dims))

    # De-interleave the binary string
    bin_str = ''.join(np.array(list(zbin_str)).reshape(n_bits, n_dims).T.flat)

    # Break up in bin numbers
    return [int(bin_str[n_bits * dim: n_bits * (dim + 1)], 2) for dim in range(n_dims)]


def hilbert_transform(c: list[int], n_bits: int, n_dims: int) -> list[int]:
    """transform coordinates for encoding using n_dims dimensional Hilbert curve.

    Args:
        c (list[int]):  Input coordinates
        n_bits (int): Bit length
        n_dims (int): Coordinate length

    Returns:
        list[int]: Output coordinates
    """

    cc = c.copy()
    m = 1 << (n_bits - 1)
    q = m
    while q > 1:
        p = q - 1
        for i in range(n_dims):
            if cc[i] & q:
                cc[0] ^= p
            else:
                t = (cc[0] ^ cc[i]) & p
                cc[0] ^= t
                cc[i] ^= t
        q >>= 1

    # Gray encoding
    for i in range(1, n_dims):
        cc[i] ^= cc[i - 1]
    t = 0
    q = m
    while q > 1:
        if cc[n_dims - 1] & q:
            t ^= q - 1
        q >>= 1
    for i in range(n_dims):
        cc[i] ^= t

    return cc


def hilbert_restore(h: list[int], n_bits: int, n_dims: int) -> list[int]:
    """Restore coordinates after decoding using n_dims dimensional Hilbert curve.

    See HilbertFlattener class for references.

    Args:
        h (list[int]):  Input coordinates
        n_bits (int): Bit length
        n_dims (int): Coordinate length

    Returns:
        list[int]: Output coordinates
    """

    hh = h.copy()
    z = 2 << (n_bits - 1)

    # Gray decoding by H ^ (H/2)
    t = hh[n_dims - 1] >> 1
    for i in range(n_dims - 1, 0, -1):
        hh[i] ^= hh[i - 1]
    hh[0] ^= t

    q = 2
    while q != z:
        p = q - 1
        for i in range(n_dims - 1, -1, -1):
            if hh[i] & q:
                hh[0] ^= p
            else:
                t = (hh[0] ^ hh[i]) & p
                hh[0] ^= t
                hh[i] ^= t
        q <<= 1

    return hh


def soi_rotations(n_side: int = 1):
    """Generator for uniformly spaced rotation matrices based on SOI algorithm.

    See SOIMultiplexer class for references.

    Args:
        n_side (int): HEALPix resolution, yields 12 * n_side ** 2 S2 points, 6 * n_side points

    Yields:
        Rotation: rotation objects from scipy.spatial.transform

    """

    if n_side < 1:
        raise ValueError('Invalid resolution parameter in soi_rotations, must be >= 1')

    # Set number of S2 (HEALPix) and S1 multiplex grid points
    n_s2 = 12 * n_side ** 2
    n_s1 = 6 * n_side

    # S1 resolution
    s1_res = 2. * np.pi / n_s1

    # Use NEST ordering for S2, see https://healpy.readthedocs.io/en/latest/healpy_pix.html
    thetas, phis = hp.pix2ang(n_side, range(n_s2), nest=True)
    psis = np.linspace(0.5 * s1_res, 2. * np.pi - 0.5 * s1_res, n_s1)

    for theta, phi in zip(thetas, phis):
        for psi in psis:
            costheta2 = np.cos(0.5 * theta)
            sintheta2 = np.sin(0.5 * theta)

            # Compute quaternion
            x1 = costheta2 * np.cos(0.5 * psi)
            x2 = costheta2 * np.sin(0.5 * psi)
            x3 = sintheta2 * np.cos(phi + 0.5 * psi)
            x4 = sintheta2 * np.sin(phi + 0.5 * psi)
            quat = [x1, x2, x3, x4]

            yield Rotation.from_quat(quat)
