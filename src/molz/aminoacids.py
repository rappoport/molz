# -*- coding: utf-8 -*-
"""Aminoacid type encodings."""

# Protein feature matrix (PFM) from A. Kidera, Y. Konishi, M. Oka, T. Ooi, H. A. Scheraga,
# Statistical Analysis of the Physical Properties of the 20 Naturally Occurring Amino Acids,
# J. Protein Chem., 1985, 4, 23. https://doi.org/10.1007/bf01025492.
# Data (10D) are derived from repeated PCA decompositions of physical properties and normalized
_aa_pfm = {
    'ALA': [-1.56, -1.67, -0.97, -0.27, -0.93, -0.78, -0.20, -0.08, 0.21, -0.48],
    'ARG': [0.22, 1.27, 1.37, 1.87, -1.70, 0.46, 0.92, -0.39, 0.23, 0.93],
    'ASN': [1.14, -0.07, -0.12, 0.81, 0.18, 0.37, -0.09, 1.23, 1.10, -1.73],
    'ASP': [0.58, -0.22, -1.58, 0.81, -0.92, 0.15, -1.52, 0.47, 0.76, 0.70],
    'CYS': [0.12, -0.89, 0.45, -1.05, -0.71, 2.41, 1.52, -0.69, 1.13, 1.10],
    'GLN': [-0.47, 0.24, 0.07, 1.10, 1.10, 0.59, 0.84, -0.71, -0.03, -2.33],
    'GLU': [-1.45, 0.19, -1.61, 1.17, -1.31, 0.40, 0.04, 0.38, -0.35, -0.12],
    'GLY': [1.46, -1.96, -0.23, -0.16, 0.10, -0.11, 1.32, 2.36, -1.66, 0.46],
    'HIS': [-0.41, 0.52, -0.28, 0.28, 1.61, 1.01, -1.85, 0.47, 1.13, 1.63],
    'ILE': [-0.73, -0.16, 1.79, -0.77, -0.54, 0.03, -0.83, 0.51, 0.66, -1.78],
    'LEU': [-1.04, 0.00, -0.24, -1.10, -0.55, -2.05, 0.96, -0.76, 0.45, 0.93],
    'LYS': [-0.34, 0.82, -0.23, 1.70, 1.54, -1.62, 1.15, -0.08, -0.48, 0.60],
    'MET': [-1.40, 0.18, -0.42, -0.73, 2.00, 1.52, 0.26, 0.11, -1.27, 0.27],
    'PHE': [-0.21, 0.98, -0.36, -1.43, 0.22, -0.81, 0.67, 1.10, 1.71, -0.44],
    'PRO': [2.06, -0.33, -1.15, -0.75, 0.88, -0.45, 0.30, -2.30, 0.74, -0.28],
    'SER': [0.81, -1.08, 0.16, 0.42, -0.21, -0.43, -1.89, -1.15, -0.97, -0.23],
    'THR': [0.26, -0.70, 1.21, 0.63, -0.10, 0.21, 0.24, -1.15, -0.56, 0.19],
    'TRP': [0.30, 2.10, -0.72, -1.57, -1.16, 0.57, -0.48, -0.40, -2.30, -0.60],
    'TYR': [1.38, 1.48, 0.80, -0.56, -0.00, -0.68, -0.31, 1.03, -0.05, 0.53],
    'VAL': [-0.74, -0.71, 2.04, -0.40, 0.50, -0.81, -1.07, 0.06, -0.46, 0.65],
}

# Li-Koehl 3D/5D representations of aminoacids based on the PCA decomposition of the BLOSUM62
# aminoacid substitution matrix from J. Li, P. Koehl, 3D representations of amino acids -—
# applications to protein sequence comparison and classification, Comput. Struct. Biotechn. J.,
# 2014, 11, 47. https://doi.org/10.1016/j.csbj.2014.09.001.
# The features are sorted in order of decreasing contribution to total variance, therefore
# suitable for converting to numerical values. 3D representation uses only the first 3
# principal components (>80% variance explained), 5D representation uses all.
_aa_lk = {
    'ALA': [0.189, -3.989, 1.989, 0.14, 1.009],
    'ARG': [5.007, 0.834, -2.709, -2.027, 3.696],
    'ASN': [7.616, 0.943, 0.101, 3.308, 0.207],
    'ASP': [7.781, 0.03, 1.821, 1.376, -3.442],
    'CYS': [-5.929, -4.837, 6.206, 2.884, 5.365],
    'GLN': [5.48, 1.293, -3.091, -2.348, 1.628],
    'GLU': [7.444, 1.005, -2.121, -1.307, -1.011],
    'GLY': [4.096, 0.772, 7.12, 0.211, -1.744],
    'HIS': [3.488, 6.754, -2.703, 4.989, 0.452],
    'ILE': [-7.883, -4.9, -2.23, 0.99, -2.316],
    'LEU': [-7.582, -3.724, -2.74, -0.736, -0.208],
    'LYS': [5.665, -0.166, -2.643, -2.808, 2.474],
    'MET': [-5.2, -2.547, -3.561, -1.73, 0.859],
    'PHE': [-8.681, 4.397, -0.732, 1.883, -1.987],
    'PRO': [4.281, -2.932, 2.319, -3.269, -4.451],
    'SER': [4.201, -1.948, 1.453, 1.226, 1.014],
    'THR': [0.774, -3.192, 0.666, 0.07, 0.407],
    'TRP': [-8.492, 9.958, 4.874, -5.288, 0.672],
    'TYR': [-6.147, 7.59, -2.065, 2.413, -0.562],
    'VAL': [-6.108, -5.341, -1.953, 0.025, -2.062],
}

_aa_lk_3d = {aa: data[0:3] for aa, data in _aa_lk.items()}

# BLOMAP encoding (5D) based on multidimensional scaling applied to the distance
# definition based on aminoacid substitution matrix (BLOSUM62) from S. Maetschke,
# M. W. Towsey, M. Boden, Blomap: an encoding of amino acids which improves signal
# peptide cleavage site prediction. In Y.-P. P. Chen, L. Wong (Eds.), Proceedings
# Third Asia Pacific Bioinformatics Conference, 1, 141. BLOMAP_3D uses the first
# 3 dimensions.
_aa_blomap = {
    'ALA': [-0.57, 0.39, -0.96, -0.61, -0.69],
    'ARG': [-0.40, -0.83, -0.61, 1.26, -0.28],
    'ASN': [-0.70, -0.63, -1.47, 1.02, 1.06],
    'ASP': [-1.62, -0.52, -0.67, 1.02, 1.47],
    'CYS': [0.07, 2.04, 0.65, -1.13, -0.39],
    'GLN': [-0.05, -1.50, -0.67, 0.49, 0.21],
    'GLU': [-0.64, -1.59, -0.39, 0.69, 1.04],
    'GLY': [-0.90, 0.87, -0.36, 1.08, 1.95],
    'HIS': [0.73, -0.67, -0.42, 1.13, 0.99],
    'ILE': [0.59, 0.79, 1.44, -1.90, -0.93],
    'LEU': [0.65, 0.84, 1.25, -0.99, -1.90],
    'LYS': [-0.64, -1.19, -0.65, 0.68, -0.13],
    'MET': [0.76, 0.05, 0.06, -0.62, -1.59],
    'PHE': [1.87, 1.04, 1.28, -0.61, -0.16],
    'PRO': [-1.82, -0.63, 0.32, 0.03, 0.68],
    'SER': [-0.39, -0.27, -1.51, -0.25, 0.31],
    'THR': [-0.04, -0.30, -0.82, -1.02, -0.04],
    'TRP': [1.38, 1.69, 1.91, 1.07, -0.05],
    'TYR': [1.75, 0.11, 0.65, 0.21, -0.41],
    'VAL': [-0.02, 0.30, 0.97, -1.55, -1.16]
}

_aa_blomap_3d = {aa: data[0:3] for aa, data in _aa_blomap.items()}

# Z scales based on PCA of 26 physicochemical descriptors
# of 87 amino acids by M. Sandberg, L. Eriksson, J. Jonsson, M. Sjöström, S. Wold,
# J. Med. Chem., 1998, 41, 2481. https://doi.org/10.1021/jm9700575.
_aa_zscale = {
    'ALA': [0.24, -2.32, 0.60, -0.14, 1.30],
    'ARG': [3.52, 2.50, -3.50, 1.99, -0.17],
    'ASN': [3.05, 1.62, 1.04, -1.15, 1.61],
    'ASP': [3.98, 0.93, 1.93, -2.46, 0.75],
    'CYS': [0.84, -1.67, 3.71, 0.18, -2.65],
    'GLN': [1.75, 0.50, -1.44, -1.34, 0.66],
    'GLU': [3.11, 0.26, -0.11, -3.04, -0.25],
    'GLY': [2.05, -4.06, 0.36, -0.82, -0.38],
    'HIS': [2.47, 1.95, 0.26, 3.90, 0.09],
    'ILE': [-3.89, -1.73, -1.71, -0.84, 0.26],
    'LEU': [-4.28, -1.30, -1.49, -0.72, 0.84],
    'LYS': [2.29, 0.89, -2.49, 1.49, 0.31],
    'MET': [-2.85, -0.22, 0.47, 1.94, -0.98],
    'PHE': [-4.22, 1.94, 1.06, 0.54, -0.62],
    'PRO': [-1.66, 0.27, 1.84, 0.70, 2.00],
    'SER': [2.39, -1.07, 1.15, -1.39, 0.67],
    'THR': [0.75, -2.18, -1.12, -1.46, -0.40],
    'TRP': [-4.36, 3.94, 0.59, 3.44, -1.59],
    'TYR': [-2.54, 2.44, 0.43, 0.04, -1.47],
    'VAL': [-2.59, -2.64, -1.54, -0.85, -0.02],
}

# Five-factor encoding of W. R. Atchley, J. Zhao, A. D. Fernandes, T. Drueke
# Proc. Natl. Acad. Sci., 2005, 102, 6395. https://doi.org/10.1073/pnas.0408677102.
_aa_azfd = {
    'ALA': [-0.591, -1.302, -0.733, 1.570, -0.146],
    'ARG': [1.538, -0.055, 1.502, 0.440, 2.897],
    'ASN': [0.945, 0.828, 1.299, -0.169, 0.933],
    'ASP': [1.050, 0.302, -3.656, -0.259, -3.242],
    'CYS': [-1.343, 0.465, -0.862, -1.020, -0.255],
    'GLN': [0.931, -0.179, -3.005, -0.503, -1.853],
    'GLU': [1.357, -1.453, 1.477, 0.113, -0.837],
    'GLY': [-0.384, 1.652, 1.330, 1.045, 2.064],
    'HIS': [0.336, -0.417, -1.673, -1.474, -0.078],
    'ILE': [-1.239, -0.547, 2.131, 0.393, 0.816],
    'LEU': [-1.019, -0.987, -1.505, 1.266, -0.912],
    'LYS': [1.831, -0.561, 0.533, -0.277, 1.648],
    'MET': [-0.663, -1.524, 2.219, -1.005, 1.212],
    'PHE': [-1.006, -0.590, 1.891, -0.397, 0.412],
    'PRO': [0.189, 2.081, -1.628, 0.421, -1.392],
    'SER': [-0.228, 1.399, -4.760, 0.670, -2.647],
    'THR': [-0.032, 0.326, 2.213, 0.908, 1.313],
    'TRP': [-0.595, 0.009, 0.672, -2.128, -0.184],
    'TYR': [0.260, 0.830, 3.097, -0.838, 1.512],
    'VAL': [-1.337, -0.279, -0.544, 1.242, -1.262],
}

# Binary encoding (10D) scheme based on empirical groupings of aminoacids from
# M. J. Zvelebil, G. J. Barton, W. R. Taylor, M. E. Sternberg, Prediction of Protein
# Secondary Structure and Active Sites using the Alignment of Homologous Sequences,
# J. Mol. Biol., 1987, 195, 957. https://doi.org/10.1016/0022-2836(87)90501-8.
_aa_zbts = {
    'ALA': [1, 0, 0, 0, 0, 1, 1, 0, 0, 0],
    'ARG': [0, 1, 0, 1, 1, 0, 0, 0, 0, 0],
    'ASN': [0, 0, 0, 1, 0, 1, 0, 0, 0, 0],
    'ASP': [0, 0, 1, 1, 1, 1, 0, 0, 0, 0],
    'CYS': [1, 0, 0, 0, 0, 1, 0, 0, 0, 0],
    'GLN': [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
    'GLU': [0, 0, 1, 1, 1, 0, 0, 0, 0, 0],
    'GLY': [1, 0, 0, 0, 0, 1, 1, 0, 0, 0],
    'HIS': [1, 1, 0, 1, 1, 0, 0, 0, 1, 0],
    'ILE': [1, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    'LEU': [1, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    'LYS': [1, 1, 0, 1, 1, 0, 0, 0, 0, 0],
    'MET': [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'PHE': [1, 0, 0, 0, 0, 0, 0, 0, 1, 0],
    'PRO': [0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
    'SER': [0, 0, 0, 1, 0, 1, 1, 0, 0, 0],
    'THR': [1, 0, 0, 1, 0, 1, 0, 0, 0, 0],
    'TRP': [1, 0, 0, 1, 0, 0, 0, 0, 1, 0],
    'TYR': [1, 0, 0, 1, 0, 0, 0, 0, 1, 0],
    'VAL': [1, 0, 0, 0, 0, 1, 0, 1, 0, 0]
}

# Modified Li-Koehl encoding based on the 1-bit discrete representation
# of the Li-Koehl 5D encoding (>= 0 -> 1, < 0 -> 0),
# with adjustments to remove duplicates:
# ASP: 30 -> 32, ALA: 23 -> 21, SER: 23 -> 22, VAL: 2 -> 3, GLY: 30 -> 29,
# ARG: 25 -> 26, PHE: 10 -> 11
_aa_lk_modified = {
    'ALA': [21],
    'ARG': [26],
    'ASN': [31],
    'ASP': [30],
    'CYS': [7],
    'GLN': [25],
    'GLU': [24],
    'GLY': [29],
    'HIS': [27],
    'ILE': [2],
    'LEU': [0],
    'LYS': [17],
    'MET': [1],
    'PHE': [11],
    'PRO': [20],
    'SER': [22],
    'THR': [23],
    'TRP': [13],
    'TYR': [10],
    'VAL': [3]
}

# Modified z scale of Sandberg et al. based on the 1-bit discretization
# (>= 0 -> 1, < 0 -> 0) of the z scale in the order z1, ..., z5
# with adjustments to remove duplicates:
# ALA: 21 -> 20, ASP: 29 -> 28, GLY: 20 -> 19, LEU: 1 -> 2, TRP: 14 -> 12
# TYR: 14 -> 13
_aa_zscale_modified = {
    'ALA': [20],
    'ARG': [26],
    'ASN': [29],
    'ASP': [28],
    'CYS': [22],
    'GLN': [25],
    'GLU': [24],
    'GLY': [19],
    'HIS': [31],
    'ILE': [1],
    'LEU': [2],
    'LYS': [27],
    'MET': [6],
    'PHE': [14],
    'PRO': [15],
    'SER': [21],
    'THR': [16],
    'TRP': [12],
    'TYR': [13],
    'VAL': [0],
}

# Modified AZFD encoding of Atchley et al. based on the 1-bit discretization
# (>= 0 -> 1, < 0 -> 0) of the AZFD encoding with adjustments to remove
# duplicates:
# ALA: 2 -> 1, LEU: 2 -> 3, MET: 5 -> 6, THR: 15 -> 14, HIS: 16 -> 17
# TYR: 29 -> 28
_aa_azfd_modified = {
    'ALA': [1],
    'ARG': [23],
    'ASN': [29],
    'ASP': [24],
    'CYS': [8],
    'GLN': [16],
    'GLU': [22],
    'GLY': [15],
    'HIS': [17],
    'ILE': [7],
    'LEU': [3],
    'LYS': [21],
    'MET': [6],
    'PHE': [5],
    'PRO': [26],
    'SER': [10],
    'THR': [14],
    'TRP': [12],
    'TYR': [28],
    'VAL': [2],
}

_aa_one2three = {
    'A': 'ALA',
    'R': 'ARG',
    'N': 'ASN',
    'D': 'ASP',
    'C': 'CYS',
    'E': 'GLU',
    'Q': 'GLN',
    'G': 'GLY',
    'H': 'HIS',
    'I': 'ILE',
    'L': 'LEU',
    'K': 'LYS',
    'M': 'MET',
    'F': 'PHE',
    'P': 'PRO',
    'S': 'SER',
    'T': 'THR',
    'W': 'TRP',
    'Y': 'TYR',
    'V': 'VAL'
}

_aa_three2one = {three: one for one, three in _aa_one2three.items()}


def aminoacid_code(sym, code: str, n_bits: int, n_pad_bits: int = 0) -> int:
    """Get aminoacid encoding from three- or one-letter code.
    Uses PFM, LK (full/3D), ZBTS, and BLOMAP (full/3D) encodings.

    Args:
        sym (str): Aminoacid three- or one-letter code
        code (str): Encoding type
        n_bits (int): Number of bits per coordinate
        n_pad_bits (int): Number of bits to pad

    Returns:
        int: Aminoacid encoding

    """

    from .utils import discretize, interleave

    if code == 'pfm':
        width = 5.
        offset = 2.5
        n_dims = 10
        table = _aa_pfm
    elif code == 'lk':
        width = 20.
        offset = 10.
        n_dims = 5
        table = _aa_lk
    elif code == 'lk_3d':
        width = 20.
        offset = 10.
        n_dims = 3
        table = _aa_lk_3d
    elif code == 'zbts':
        width = 1.
        offset = 0.
        n_dims = 10
        table = _aa_zbts
    elif code == 'blomap':
        width = 5.
        offset = 2.5
        n_dims = 5
        table = _aa_blomap
    elif code == 'blomap_3d':
        width = 5.
        offset = 2.5
        n_dims = 3
        table = _aa_blomap_3d
    elif code == 'zscale':
        width = 10.
        offset = 5.
        n_dims = 5
        table = _aa_zscale
    elif code == 'azfd':
        width = 10.
        offset = 5.
        n_dims = 5
        table = _aa_azfd
    elif code == 'lk_modified':
        width = 32.
        offset = 0.
        n_dims = 1
        table = _aa_lk_modified
    elif code == 'zscale_modified':
        width = 32.
        offset = 0.
        n_dims = 1
        table = _aa_zscale_modified
    elif code == 'azfd_modified':
        width = 32.
        offset = 0.
        n_dims = 1
        table = _aa_azfd_modified
    else:
        raise ValueError(f'Invalid encoding type')

    if len(sym) == 3:
        aa = sym
    elif len(sym) == 1:
        aa = _aa_one2three[sym]
    else:
        raise ValueError(f'Invalid aminoacid code')

    n_bins = 2 ** n_bits
    bins = [discretize(coord, width, offset, n_bins) for coord in table[aa]]
    return interleave(bins, n_bits, n_dims) * 2 ** n_pad_bits
