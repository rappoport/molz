# -*- coding: utf-8 -*-
"""3D molecule representations."""

from .discretizer import Discretizer
from .flattener import Flattener
from .molecule_z import MoleculeZ
from .utils import xyz2cart, cart2xyz, transpose, sph2cart, transform_3d, \
    split_elements, combine_elements, get_min_max


class Molecule3D:
    """Base class for 3D molecule representations.

    Attributes:
        coord_type (str): Coordinate type ('spherical')
        has_elements (bool): Include element identifier
        coord (list[list[float]]): Molecular 3D coordinates
        elements (list[float]): Element identifiers (optional)
        n_dims (int): Dimensionality

    """

    coord_type = None
    has_elements = None
    coord = None
    elements = None
    n_dims = None

    def __new__(cls, coord: list[list[float]] = None, coord_type: str = 'spherical',
                has_elements: bool = True, *args, **kwargs) -> "Molecule3D":
        """Factory function based on coord_type and has_elements args.

        Args:
            coord (list[list[float]]): Input 3D coordinates
            has_elements (bool): Include element identifier
            coord_type (str): Coordinate type ('cartesian' or 'spherical')

        Returns:
            Molecule3D: New 3D coordinate object

        """

        if cls is Molecule3D:
            if coord_type == 'cartesian':
                if has_elements:
                    cls_new = ElementCartesian3D
                else:
                    cls_new = Cartesian3D
            elif coord_type == 'spherical':
                if has_elements:
                    cls_new = ElementSpherical3D
                else:
                    cls_new = Spherical3D
            else:
                raise ValueError(f'Unknown coordinate type {coord_type}')
        else:
            raise ValueError(f'Unknown 3D coordinate class {cls}')

        return super().__new__(cls_new)

    @classmethod
    def from_xyz(cls, s: str, coord_type: str = 'spherical', has_elements: bool = True,
                 to_inertial_frame: bool = True, **kwargs) -> "Molecule3D":
        """Initialize molecular 3D coordinates from string in XYZ format.

        Args:
            s (str): Input string in XYZ format
            coord_type (str): Type of molecular 3D coordinates
            has_elements (bool): Include element identifier
            to_inertial_frame (bool): Transform into molecule-fixed coordinate frame

        Returns:
            Molecule3D: Molecular 3D coordinates

        """

        coord = xyz2cart(s)
        return cls(coord,
                   coord_type=coord_type,
                   has_elements=has_elements,
                   input_type='cartesian',
                   to_inertial_frame=to_inertial_frame,
                   **kwargs)

    def to_xyz(self) -> str:
        """Write coordinates as string in XYZ format."""
        raise NotImplementedError

    def encode(self, grid_type: str = 'uniform', coding_type: str = 'morton',
               n_bits: int = 8, **kwargs) -> MoleculeZ:
        """Encode to flattened molecular coordinates.

        Args:
            grid_type (str): Type of grid discretization
            coding_type (str): Type of multidimensional coding
            n_bits (int): Bit width

        Returns:
            MoleculeZ: Flattened coordinates

        """

        disc = Discretizer(coord_type=self.coord_type,
                           has_elements=self.has_elements,
                           grid_type=grid_type,
                           n_bits=n_bits,
                           **kwargs)
        flat = Flattener(coding_type=coding_type,
                         n_bits=n_bits,
                         n_dims=self.n_dims,
                         **kwargs)
        coord_z = [flat.flatten(disc.discretize(atom)) for atom in self.coord]
        mol_z = MoleculeZ(coord_z,
                          self.elements,
                          coord_type=self.coord_type,
                          has_elements=self.has_elements,
                          grid_type=grid_type,
                          coding_type=coding_type,
                          n_bits=n_bits,
                          n_dims=self.n_dims,
                          **kwargs)
        return mol_z

    @classmethod
    def decode(cls, mol_z: MoleculeZ, grid_type: str = 'uniform', coding_type: str = 'morton',
               **kwargs) -> "Molecule3D":
        """Decode from flattened molecular coordinates.

        Args:
            mol_z (MoleculeZ): Flattened coordinates
            grid_type (str): Grid type
            coding_type (str): Coding type

        Returns:
            Molecule3D: Molecular 3D coordinates

        """

        if cls.coord_type is not None and cls.coord_type != mol_z.coord_type:
            raise ValueError(f'Incompatible coordinate type: decoder: {cls.coord_type}, '
                             f'coordinates: {mol_z.coord_type}')
        if cls.has_elements is not None and cls.has_elements != mol_z.has_elements:
            raise ValueError(f'Incompatible element identifier handling: decoder: {cls.has_elements}, '
                             f'coordinates: {mol_z.has_elements}')

        if grid_type != mol_z.grid_type:
            raise ValueError(f'Incompatible grid type: decoder: {grid_type}, ',
                             f'coordinates: {mol_z.grid_type}')

        if coding_type != mol_z.coding_type:
            raise ValueError(f'Incompatible coding type: decoder: {coding_type}, ',
                             f'coordinates: {mol_z.coding_type}')

        mol_3d = mol_z.decode(**kwargs)
        return mol_3d


class Cartesian3D(Molecule3D):
    """3D molecule representation in Cartesian coordinates.

    Attributes:
        coord_type (str): Coordinate type ('spherical')
        has_elements (bool): Include element identifier in coord (False)
        coord (list[list[float]]): Molecular 3D coordinates
        elements (list[float]): Element identifiers
        n_dims (int): Dimensionality (3)

    """

    coord_type = 'cartesian'
    has_elements = False
    n_dims = 3

    def __init__(self, coord: list[list[float]] = None, input_type: str = 'cartesian',
                 to_inertial_frame: bool = False, **kwargs):
        """Initializer for Cartesian3D objects.

        Args:
            coord (list[list[float]]): Input molecular 3D coordinates
            input_type (str): Type of input molecular 3D coordinates
            to_inertial_frame (bool): Transform into molecule-fixed coordinate frame

        """

        coord = transform_3d(coord,
                             coord_type='cartesian',
                             input_type=input_type,
                             to_inertial_frame=to_inertial_frame)
        self.elements, self.coord = split_elements(coord)

    def to_xyz(self) -> str:
        """Write coordinates as string in XYZ format.

        Returns:
            str: Output string in XYZ format
        """

        cart = combine_elements(self.elements, self.coord)
        return cart2xyz(cart)


class Spherical3D(Molecule3D):
    """3D molecule representation in spherical coordinates with element.

        Attributes:
            coord_type (str): Coordinate type ('spherical')
            has_elements (bool): Include element identifier in coord (False)
            coord (list[list[float]]): Molecular 3D coordinates
            elements (list[float]): Element identifiers
            n_dims (int): Dimensionality (3)

    """

    coord_type = 'spherical'
    has_elements = False
    n_dims = 3

    def __init__(self, coord: list[list[float]] = None, input_type: str = 'spherical',
                 to_inertial_frame: bool = False, **kwargs):
        """Initializer for Spherical3D objects.

        Args:
            coord (list[list[float]]): Input molecular 3D coordinates
            input_type (str): Type of input molecular 3D coordinates
            to_inertial_frame (bool): Transform into molecule-fixed coordinate frame

        """

        coord = transform_3d(coord,
                             coord_type='spherical',
                             input_type=input_type,
                             to_inertial_frame=to_inertial_frame)
        self.elements, self.coord = split_elements(coord)

    def to_xyz(self) -> str:
        """Write coordinates as string in XYZ format.

        Returns:
            str: Output string in XYZ format
        """

        rad, theta, phi = transpose(*self.coord)
        x, y, z = sph2cart(rad, theta, phi)
        cart = combine_elements(self.elements, transpose(x, y, z))
        return cart2xyz(cart)


class ElementCartesian3D(Molecule3D):
    """3D molecule representation in Cartesian coordinates with element.

    Attributes:
        coord_type (str): Coordinate type ('cartesian')
        has_elements (bool): Include element identifier in coord (True)
        coord (list[list[float]]): Molecular 3D coordinates
        elements (list[float]): Element identifiers (empty)
        n_dims (int): Dimensionality (4)

    """

    coord_type = 'cartesian'
    has_elements = True
    elements = None
    n_dims = 4

    def __init__(self, coord: list[list[float]] = None, input_type: str = 'cartesian',
                 to_inertial_frame: bool = False, **kwargs):
        """Initializer for ElementCartesian3D objects.

        Args:
            coord (list[list[float]]): Input molecular 3D coordinates
            input_type (str) : Type of input molecular 3D coordinates
            to_inertial_frame (bool): Transform into molecule-fixed coordinate frame

        """

        self.coord = transform_3d(coord,
                                  coord_type='cartesian',
                                  input_type=input_type,
                                  to_inertial_frame=to_inertial_frame)

    def to_xyz(self) -> str:
        """Write coordinates as string in XYZ format.

        Returns:
            str: Output string in XYZ format

        """

        return cart2xyz(self.coord)


class ElementSpherical3D(Molecule3D):
    """3D molecule representation in spherical polar coordinates with element.

    Attributes:
        coord_type (str): Coordinate type ('spherical')
        has_elements (bool): Include element identifier in coord (True)
        coord (list[list[float]]): Molecular 3D coordinates
        elements (list[float]): Element identifiers (empty)
        n_dims (int): Dimensionality (4)

    """

    coord_type = 'spherical'
    has_elements = True
    elements = None
    n_dims = 4

    def __init__(self, coord: list[list[float]] = None, input_type: str = 'spherical',
                 to_inertial_frame: bool = False, **kwargs):
        """Initializer for ElementSpherical3D objects.

        Args:
            coord (list[list[float]]): Input molecular 3D coordinates
            input_type (str): Type of input molecular 3D coordinates
            to_inertial_frame (bool): Transform into molecule-fixed coordinate frame

        """

        self.coord = transform_3d(coord,
                                  coord_type='spherical',
                                  input_type=input_type,
                                  to_inertial_frame=to_inertial_frame)

    def to_xyz(self) -> str:
        """Write coordinates as string in XYZ format.

        Returns:
            str: Output string in XYZ format

        """

        el, rad, theta, phi = transpose(*self.coord)
        x, y, z = sph2cart(rad, theta, phi)
        cart = transpose(el, x, y, z)
        return cart2xyz(cart)
