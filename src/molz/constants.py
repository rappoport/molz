# -*- coding: utf-8 -*-
"""Package-wide constants."""

import numpy as np

# au (Bohr) -> Angstrom conversion
au2aa = 0.52917721092

# Threshold for numerical comparisons
thr = np.finfo(float).eps * 100.0
