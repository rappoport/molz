# -*- coding: utf-8 -*-
"""Conversions between element names and masses."""

from typing import Union

# Element name lookup by number
_el_num2name = [
    'Q',
    'H', 'He',
    'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne',
    'Na', 'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar',
    'K', 'Ca', 'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr',
    'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', 'I', 'Xe',
    'Cs', 'Ba', 'La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf', 'Ta',
    'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn',
    'Fr', 'Ra', 'Ac', 'Th', 'Pa', 'U', 'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es', 'Fm', 'Md', 'No', 'Lr', 'Rf', 'Db',
    'Sg', 'Bh', 'Hs', 'Mt', 'Ds', 'Rg'
]

# Element number lookup by name
_el_name2num = {el: num for num, el in enumerate(_el_num2name)}

# Element mass by number
_el_num2mass = [
    0., 1.00794, 4.002602, 6.941, 9.012182, 10.811, 12.011, 14.00674, 15.9994,
    18.9984032, 20.1797, 22.989768, 24.3050, 26.981539, 28.0855, 30.973762,
    32.066, 35.4527, 39.948, 39.0983, 40.078, 44.955910, 47.88, 50.9415, 51.9961,
    54.93805, 55.847, 58.93320, 58.6934, 63.546, 65.39, 69.723, 72.61, 74.92159,
    78.96, 79.904, 83.80, 85.4678, 87.62, 88.90585, 91.224, 92.90638, 95.94, 98,
    101.07, 102.90550, 106.42, 107.8682, 112.411, 114.82, 118.710, 121.757, 127.60,
    126.90447, 131.29, 132.90543, 137.327, 138.9055, 140.115, 140.90765, 144.24, 145,
    150.36, 151.965, 157.25, 158.92534, 162.50, 164.93032, 167.26, 168.93421, 173.04,
    174.967, 178.49, 180.9479, 183.85, 186.207, 190.2, 192.22, 195.08, 196.96654,
    200.59, 204.3833, 207.2, 208.98037, 209., 210., 222., 223., 226.025, 227.028,
    232.0381, 231.0359, 238.0289, 237.048, 244., 243., 247., 247., 251., 252., 257., 258.,
    259., 262., 261., 262., 263., 262., 265., 266.
]

# Element mass by name
_el_name2mass = {_el_num2name[num]: mass for num, mass in enumerate(_el_num2mass)}


def element(inp: Union[str, int]) -> Union[str, int]:
    """Element number to name and name to number lookups.

    Args:
        inp (Union[str, int]): Element name or element number

    Returns:
        Union[str, int]: Element number or name

    """

    if isinstance(inp, int):
        return _el_num2name[inp]
    elif isinstance(inp, str):
        return _el_name2num[inp]


def element_mass(inp: Union[str, int]) -> float:
    """Element mass lookups by number or name.

    Args:
        inp (Union[str, int]): Element name or element number

    Returns:
        Union[str, int]: Element mass in amu

    """

    if isinstance(inp, int):
        return _el_num2mass[inp]
    elif isinstance(inp, str):
        return _el_name2mass[inp]
