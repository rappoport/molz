# -*- coding: utf-8 -*-
"""Flatteners and unflatteners for 3D coordinates using space-filling curves."""

from .utils import interleave, deinterleave, hilbert_transform, \
    hilbert_restore


class Flattener:
    """Base class for flatteners for molecular 3D coordinates.

    Attributes:
        coding_type (str): Type of multidimensional coding
        n_bits (int): Bit width
        n_dims (int): Number of dimensions

    """

    cording_type = None
    n_bits = 0
    n_dims = 0

    def __new__(cls, coding_type: str = 'morton', *args, **kwargs) -> "Flattener":
        """Factory function based on coding_type arg.

        Args:
            coding_type (str): Type of multidimensional coding

        Returns:
            Flattener: New flattener object

        """

        if cls is Flattener:
            if coding_type == 'morton':
                cls_new = MortonFlattener
            elif coding_type == 'hilbert':
                cls_new = HilbertFlattener
            else:
                raise ValueError(f'Unknown coding type {coding_type}')
        else:
            raise ValueError(f'Unknown flattener class {cls}')

        return super().__new__(cls_new)

    def __init__(self, n_bits: int = 8, n_dims: int = 4, **kwargs):

        self.n_bits = n_bits
        self.n_dims = n_dims

    def flatten(self, atom_bin: list[int]) -> int:
        """Flatten one set of discretized 3D coordinates."""
        raise NotImplementedError

    def unflatten(self, atom_z: int) -> list[int]:
        """Unflatten one set of flattened coordinates."""
        raise NotImplementedError


class MortonFlattener(Flattener):
    """Morton order curve flattener."""

    coding_type = 'morton'

    def flatten(self, atom_bin: list[int]) -> int:
        """Flatten one set of discretized 3D coordinates.

        Args:
            atom_bin (list[int]): Discretized 3D coordinates

        Returns:
            int: Flattened coordinates

        """

        return interleave(atom_bin, self.n_bits, self.n_dims)

    def unflatten(self, atom_z: int) -> list[int]:
        """Unflatten one set of flattened coordinates.

        Args:
            atom_z (int): Flattened coordinates

        Returns:
            list[int]: Discretized 3D coordinates

        """

        return deinterleave(atom_z, self.n_bits, self.n_dims)


class HilbertFlattener(Flattener):
    """Hilbert curve flattener.

    Implementation follows https://github.com/galtay/hilbertcurve
    using algorithm from J. Skilling, "Programming the Hilbert Curve",
    AIP Conf. Proc., 2004, 707, 381. https://doi.org/10.1063/1.1751381

    """

    coding_type = 'hilbert'

    def flatten(self, atom_bin: list[int]) -> int:
        """"Flatten one set of discretized 3D coordinates.

        Args:
            atom_bin (list[int]): Discretized 3D coordinates

        Returns:
            int: Flattened coordinates

        """

        c = hilbert_transform(atom_bin, self.n_bits, self.n_dims)
        return interleave(c, self.n_bits, self.n_dims)

    def unflatten(self, atom_z: int) -> list[int]:
        """Unflatten one set of coordinates.

        Args:
            atom_z (int): Flattened coordinates

        Returns:
            list[int]: Discretized 3D coordinates

        """

        h = deinterleave(atom_z, self.n_bits, self.n_dims)
        return hilbert_restore(h, self.n_bits, self.n_dims)
