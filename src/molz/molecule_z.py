# -*- coding: utf-8 -*-
"""Flattened coordinate molecule representations."""

import json
from typing import ForwardRef

import numpy as np

from .discretizer import Discretizer
from .flattener import Flattener
from .utils import combine_elements


class MoleculeZ:
    """Base class for flattened molecule representation.

    Attributes:
        coord_type (str): Coordinate type ('spherical')
        has_elements (bool): Include element identifier
        grid_type (str): Grid type ('uniform')
        coding_type (str): Type of multidimensional coding
        coord (list[float]): Flattened coordinates
        elements (list[float]): Element identifiers (optional)

    """

    coord_type = None
    has_elements = None
    grid_type = None
    coding_type = None
    coord = None
    elements = None
    n_bits = None
    n_dims = None

    def __new__(cls, coord: list[float] = None, elements: list[float] = None,
                coord_type: str = 'spherical', has_elements: bool = True,
                grid_type: str = 'uniform', coding_type: str = 'morton',
                *args, **kwargs) -> "MoleculeZ":
        """Factory function based on coord_type, has_elements, grid_type, coding_type args.

        Args:
            coord (list[float]): Flattened coordinates
            elements (list[float]): Element identifiers  (if has_elements is False)
            coord_type (str): Coordinate type ('cartesian' or 'spherical')
            has_elements (bool): Include element identifier
            grid_type (str): Grid type ('uniform')
            coding_type (str): Type of multidimensional coding

        Returns:
           MoleculeZ: New flattened coordinate object

        """

        if cls is MoleculeZ:
            if coord_type == 'cartesian':
                if has_elements:
                    if grid_type == 'uniform':
                        if coding_type == 'morton':
                            cls_new = ElementCartesianUniformMortonZ
                        elif coding_type == 'hilbert':
                            cls_new = ElementCartesianUniformHilbertZ
                        else:
                            raise ValueError(f'Unknown coding type {coding_type}')
                    else:
                        raise ValueError(f'Unknown grid type {grid_type}')
                else:
                    if grid_type == 'uniform':
                        if coding_type == 'morton':
                            cls_new = CartesianUniformMortonZ
                        elif coding_type == 'hilbert':
                            cls_new = CartesianUniformHilbertZ
                        else:
                            raise ValueError(f'Unknown coding type {coding_type}')
                    else:
                        raise ValueError(f'Unknown grid type {grid_type}')
            elif coord_type == 'spherical':
                if has_elements:
                    if grid_type == 'uniform':
                        if coding_type == 'morton':
                            cls_new = ElementSphericalUniformMortonZ
                        elif coding_type == 'hilbert':
                            cls_new = ElementSphericalUniformHilbertZ
                        else:
                            raise ValueError(f'Unknown coding type {coding_type}')
                    else:
                        raise ValueError(f'Unknown grid type {grid_type}')
                else:
                    if grid_type == 'uniform':
                        if coding_type == 'morton':
                            cls_new = SphericalUniformMortonZ
                        elif coding_type == 'hilbert':
                            cls_new = SphericalUniformHilbertZ
                        else:
                            raise ValueError(f'Unknown coding type {coding_type}')
                    else:
                        raise ValueError(f'Unknown grid type {grid_type}')
            else:
                raise ValueError(f'Unknown coordinate type {coord_type}')
        else:
            raise ValueError(f'Unknown flattened coordinate class: {cls}')

        return super().__new__(cls_new)

    def __init__(self, coord: list[float] = None, elements: list[float] = None,
                 n_bits: int = 8, **kwargs):

        if not self.has_elements and elements is None:
            raise ValueError('Element identifiers required if has_elements is False')
        if self.has_elements and elements is not None:
            raise ValueError('Element identifiers ignored if has_elements is True')

        self.coord = coord
        self.elements = elements
        self.n_bits = n_bits

    @classmethod
    def from_json(cls, s: str) -> "MoleculeZ":
        """Read from JSON-formatted string.

        Args:
            s (str): JSON-formatted string

        Returns:
            MoleculeZ: New flattened coordinates object

        """

        data = json.loads(s)
        return cls(**data)

    def to_json(self):
        """
        Convert to JSON-formatted string.

        Returns:
            str: JSON-formatted string

        """

        data = {
            'coord_type': self.coord_type,
            'has_elements': self.has_elements,
            'grid_type': self.grid_type,
            'coding_type': self.coding_type,
            'coord': self.coord,
            'elements': self.elements,
            'n_bits': self.n_bits,
            'n_dims': self.n_dims
        }
        return json.dumps(data)

    @classmethod
    def encode(cls, mol_3d: ForwardRef("Molecule3D"), grid_type: str = 'uniform',
               coding_type: str = 'morton', n_bits: int = 8, **kwargs) -> "MoleculeZ":
        """Encode Molecular 3D coordinates to flattened coordinates.

        Args:
        mol_3d (Molecule3D): Molecular 3D coordinates
        grid_type (str): Type of grid discretization
        coding_type (str): Type of multidimensional coding
        n_bits (int): Bit width

        Returns:
            MoleculeZ object

        """

        if cls.coord_type is not None and cls.coord_type != mol_3d.coord_type:
            raise ValueError(f'Incompatible coordinate type: decoder: {cls.coord_type}, '
                             f'coordinates: {mol_3d.coord_type}')
        if cls.has_elements is not None and cls.has_elements != mol_3d.has_elements:
            raise ValueError(f'Incompatible element identifier handling: decoder: {cls.has_elements}, '
                             f'coordinates: {mol_3d.has_elements}')
        mol_z = mol_3d.encode(grid_type=grid_type,
                              coding_type=coding_type,
                              n_bits=n_bits,
                              **kwargs)

        return mol_z

    def decode(self, **kwargs):
        """Decode flattened coordinates to molecular 3D coordinates.

        Returns:
            Molecule3D: New molecular 3D coordinate object

        """

        from .molecule_3d import Molecule3D

        disc = Discretizer(coord_type=self.coord_type,
                           has_elements=self.has_elements,
                           grid_type=self.grid_type,
                           n_bits=self.n_bits,
                           **kwargs)
        flat = Flattener(coding_type=self.coding_type,
                         n_bits=self.n_bits,
                         n_dims=self.n_dims,
                         **kwargs)
        coord_3d = [disc.undiscretize(flat.unflatten(atom)) for atom in self.coord]
        if not self.has_elements:
            coord_3d = combine_elements(self.elements, coord_3d)
        mol_3d = Molecule3D(coord_3d,
                            coord_type=self.coord_type,
                            has_elements=self.has_elements,
                            **kwargs)
        return mol_3d

    def distance(self, other: "MoleculeZ", dist_type: str = 'hausdorff') -> int:
        """Compute distances between sets of flattened molecular coordinates.

        Args:
        other (MoleculeZ): Other set of molecular coordinates
        dist_type (str): Type of distance measure ('hausdorff')

        Returns:
            dist (int): Distance

        """

        coord1 = np.array(self.coord)
        coord2 = np.array(other.coord)

        if dist_type == 'hausdorff':
            distances = np.abs(coord1[:, None] - coord2[None, :])
            dist = max(np.amax(np.amin(distances, axis=1)),
                       np.amax(np.amin(distances, axis=0)))
        else:
            raise ValueError(f'Unknown distance type: {dist_type}')

        return dist


class CartesianUniformMortonZ(MoleculeZ):
    """Flattened Cartesian coordinates on uniform grid with Morton coding."""

    coord_type = 'cartesian'
    has_elements = False
    grid_type = 'uniform'
    coding_type = 'morton'
    n_dims = 3


class SphericalUniformMortonZ(MoleculeZ):
    """Flattened spherical coordinates on uniform grid with Morton coding."""

    coord_type = 'spherical'
    has_elements = False
    grid_type = 'uniform'
    coding_type = 'morton'
    n_dims = 3


class CartesianUniformHilbertZ(MoleculeZ):
    """Flattened Cartesian coordinates on uniform grid with Hilbert coding."""

    coord_type = 'cartesian'
    has_elements = False
    grid_type = 'uniform'
    coding_type = 'hilbert'
    n_dims = 3


class SphericalUniformHilbertZ(MoleculeZ):
    """Flattened spherical coordinates on uniform grid with Hilbert coding."""

    coord_type = 'spherical'
    has_elements = False
    grid_type = 'uniform'
    coding_type = 'hilbert'
    n_dims = 3


class ElementCartesianUniformMortonZ(MoleculeZ):
    """Flattened Cartesian coordinates with element on uniform grid with Morton coding."""

    coord_type = 'cartesian'
    has_elements = True
    grid_type = 'uniform'
    coding_type = 'morton'
    n_dims = 4


class ElementSphericalUniformMortonZ(MoleculeZ):
    """Flattened spherical coordinates with element on uniform grid with Morton coding."""

    coord_type = 'spherical'
    has_elements = True
    grid_type = 'uniform'
    coding_type = 'morton'
    n_dims = 4


class ElementCartesianUniformHilbertZ(MoleculeZ):
    """Flattened Cartesian coordinates with element on uniform grid with Hilbert coding."""

    coord_type = 'cartesian'
    has_elements = True
    grid_type = 'uniform'
    coding_type = 'hilbert'
    n_dims = 4


class ElementSphericalUniformHilbertZ(MoleculeZ):
    """Flattened spherical coordinates with element on uniform grid with Hilbert coding."""

    coord_type = 'spherical'
    has_elements = True
    grid_type = 'uniform'
    coding_type = 'hilbert'
    n_dims = 4
