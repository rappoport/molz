# -*- coding: utf-8 -*-
"""Multiplexers and demultiplexers for 3D coordinates."""

import numpy as np

from .utils import soi_rotations


class Multiplexer:
    """Base class for multiplexers for molecular 3D coordinates.

    Attributes:
        multiplex_type (str): Type of multiplexing
        multiplex_level (int): Multiplexing grid level

    """

    multiplex_type = None
    multiplex_level = 0

    def __new__(cls, multiplex_type: str = 'identity', *args, **kwargs) -> "Multiplexer":
        """Factory function based on multiplex_type arg.

        Args:
            multiplex_type (str): Type of multiplexing

        Returns:
            Multiplexer: New multiplexer object

        """

        if cls is Multiplexer:
            if multiplex_type == 'identity':
                cls_new = IdentityMultiplexer
            elif multiplex_type == 'soi':
                cls_new = SOIMultiplexer
            else:
                raise ValueError(f'Unknown multiplex type {multiplex_type}')
        else:
            raise ValueError(f'Unknown multiplexer class {cls}')

        return super().__new__(cls_new)

    def multiplex(self, atom_3d: list[float]) -> list[list[float]]:
        raise NotImplementedError

    def demultiplex(self, images_3d: list[list[float]]) -> list[float]:
        raise NotImplementedError


class IdentityMultiplexer(Multiplexer):
    """Generate single image from input coordinates."""

    multiplex_type = 'identity'

    def multiplex(self, atom_3d: list[float]) -> list[list[float]]:
        """Return input set of 3D coordinates as single image.

        Args:
            atom_3d (list[float]): Atom 3D coordinates

        Return:
            list[list[float]]: Images 3D coordinates

        """

        return [atom_3d]

    def demultiplex(self, images_3d: list[list[float]]) -> list[float]:
        """Return coordinates of single image.

        Args:
            images_3d (list[list[float]]): Images 3D coordinates

        Return:
            list[float]: Atom 3D coordinates

        """

        return images_3d[0]


class SOIMultiplexer(Multiplexer):
    """Generate images by uniformly sampled rotations using SOI algorithm.

    Implementation follows https://mitchell-web.ornl.gov/SOI/index.php
    published by A. Yershova, S. Jain, S. M. LaValle, J. C. Mitchell,
    "Generating Uniform Incremental Grids on SO(3) Using the Hopf Fibration",
    Int. J. Robot. Res., 2010, 29, 801. https://doi.org/10.1177/0278364909352700

    The S2 grid uses the HEALPix algorithm implemented in the healpy package,
    https://github.com/healpy/healpy, based on K. M. Gorski et al.,
    "HEALPix: A Framework for High-Resolution Discretization and Fast Analysis
    of Data Distributed on the Sphere", Astrophys. J., 2005, 622, 759.
    https://doi.org/10.1086/427976
    Base implementation is at https://healpix.sourceforge.io/.

    """

    multiplex_type = 'soi'

    def __init__(self, multiplex_level: int = 0, **kwargs):

        self.multiplex_level = multiplex_level

        # Set n_side parameter for the HEALPix algorithm
        self.n_side = 2 ** multiplex_level

        # Total grid size (S2 * S1)
        self.n_grid = 72 * 8 ** multiplex_level

    def multiplex(self, atom_3d: list[float]) -> list[list[float]]:
        """Generate images by applying SOI rotations.

        Args:
            atom_3d (list[float]): Atom 3D coordinates

        Returns:
            list[list[float]]: Images 3D coordinates

        """

        return [rot.apply(atom_3d).tolist() for rot in soi_rotations(self.n_side)]

    def demultiplex(self, images_3d: list[list[float]]) -> list[float]:
        """Combine SOI images by averaging.

        Args:
            images_3d (list[list[float]]): Images 3D coordinates

        Returns:
            list[float]: Atom 3D coordinates

        """

        if len(images_3d) != self.n_grid:
            raise ValueError(
                f'Size mismatch in SOIMultiplexer. Expected {self.n_grid} images, received {len(images_3d)}')

        inv_3d = [rot.apply(image_3d, inverse=True)
                  for image_3d, rot in zip(images_3d, soi_rotations(self.n_side))]

        return np.array(inv_3d).mean(axis=0).tolist()
