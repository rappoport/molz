# -*- coding: utf-8 -*-
"""Discretizers and undiscretizers for molecular 3D coordinates."""

import numpy as np

from .utils import discretize, undiscretize


class Discretizer:
    """Base class for discretizers for molecular 3D coordinates.

    Attributes:
        coord_type (str): Coordinate type ('cartesian' or 'spherical')
        has_elements (bool): Include element identifier
        grid_type (str): Grid type ('uniform')
        n_bits (int): Bit width
        n_dims (int): Dimensionality

    """

    coord_type = None
    has_elements = None
    grid_type = None
    n_bits = 0
    n_dims = 0

    def __new__(cls, coord_type: str = 'spherical', has_elements: bool = True,
                grid_type: str = 'uniform', *args, **kwargs) -> "Discretizer":
        """Factory function based on coord_type, has_elements, grid_type args.

        Args:
            coord_type (str): Type of molecular 3D coordinates
            has_elements (bool): Include element identifier
            grid_type (str): Type of grid discretization

        Returns:
            Discretizer: New discretizer object

        """

        if cls is Discretizer:
            if coord_type == 'cartesian':
                if has_elements:
                    if grid_type == 'uniform':
                        cls_new = ElementCartesianUniformDiscretizer
                    else:
                        raise ValueError(f'Unknown grid type {grid_type}')
                else:
                    if grid_type == 'uniform':
                        cls_new = CartesianUniformDiscretizer
                    else:
                        raise ValueError(f'Unknown grid type {grid_type}')
            elif coord_type == 'spherical':
                if has_elements:
                    if grid_type == 'uniform':
                        cls_new = ElementSphericalUniformDiscretizer
                    else:
                        raise ValueError(f'Unknown grid type {grid_type}')
                else:
                    if grid_type == 'uniform':
                        cls_new = SphericalUniformDiscretizer
                    else:
                        raise ValueError(f'Unknown grid type {grid_type}')
            else:
                raise ValueError(f'Unknown coordinate type {coord_type}')
        else:
            raise ValueError(f'Unknown discretizer class {cls}')

        return super().__new__(cls_new)

    def discretize(self, atom_3d: list[float]) -> list[int]:
        """Discretize one set of 3D coordinates.

        Args:
            atom_3d (list[float]): 3D coordinates

        Returns:
            list[int]: Discretized 3D coordinates

        """
        raise NotImplementedError

    def undiscretize(self, atom_bin: list[int]) -> list[float]:
        """Undiscretize one set of 3D coordinates.

        Args:
            atom_bin (list[int]): Discretized 3D coordinates

        Returns:
            list[float]: 3D coordinates

        """
        raise NotImplementedError


class CartesianUniformDiscretizer(Discretizer):
    """Discretizer for Cartesian 3D coordinates on a uniform grid."""

    coord_type = 'cartesian'
    has_elements = False
    grid_type = 'uniform'
    n_dims = 3

    def __init__(self, n_bits: int = 8, **kwargs):

        self.n_bits = n_bits

        # Set dimensions
        self.width = kwargs.get('width', 10.)

        # Number of bins per coordinate
        n_bins = 2 ** n_bits
        self.n_bins = n_bins

    def discretize(self, atom_3d: list[float]) -> list[int]:
        """Discretize Cartesian 3D coordinates on a uniform grid.

        Args:
            atom_3d (list[float]): Atom 3D coordinates

        Returns:
            list[int]: Discretized coordinates

        """

        # Unpack Cartesian coordinates
        x, y, z = atom_3d

        # Compute bin numbers for each coordinate
        x_bin = discretize(x, self.width, 0.5 * self.width, self.n_bins)
        y_bin = discretize(y, self.width, 0.5 * self.width, self.n_bins)
        z_bin = discretize(z, self.width, 0.5 * self.width, self.n_bins)

        return [x_bin, y_bin, z_bin]

    def undiscretize(self, atom_bin: list[int]) -> list[float]:
        """ Undiscretize Cartesian 3D coordinates on a uniform grid.

        Args:
            atom_bin (list[int]): Discretized coordinates

        Returns:
            list[float]: Atom 3D coordinates and element

        """

        # Unpack discretized coordinates
        x_bin, y_bin, z_bin = atom_bin

        # Place in the center of x, y, z bin
        x = undiscretize(x_bin, self.width, 0.5 * self.width, self.n_bins)
        y = undiscretize(y_bin, self.width, 0.5 * self.width, self.n_bins)
        z = undiscretize(z_bin, self.width, 0.5 * self.width, self.n_bins)

        return [x, y, z]


class SphericalUniformDiscretizer(Discretizer):
    """Discretizer for spherical polar 3D coordinates on a uniform grid."""

    coord_type = 'spherical'
    has_elements = False
    grid_type = 'uniform'
    n_dims = 3

    def __init__(self, n_bits: int = 8, **kwargs):

        self.n_bits = n_bits

        # Set dimensions
        self.radius = kwargs.get('radius', 10.)

        # Number of bins per coordinate
        n_bins = 2 ** n_bits
        self.n_bins = n_bins

    def discretize(self, atom_3d: list[float]) -> list[int]:
        """Discretize spherical 3D coordinates on a uniform grid.

        Args:
            atom_3d (list[float]): Atom 3D coordinates

        Returns:
            list[int]: Discretized coordinates

        """

        # Unpack spherical polar coordinates
        r, t, p = atom_3d

        # Compute bin numbers for each coordinate
        r_bin = discretize(r, self.radius, 0., self.n_bins)
        t_bin = discretize(t, np.pi, 0., self.n_bins)
        p_bin = discretize(p, 2. * np.pi, np.pi, self.n_bins)

        return [r_bin, t_bin, p_bin]

    def undiscretize(self, atom_bin: list[int]) -> list[float]:
        """Undiscretize spherical 3D coordinates on a uniform grid.

       Args:
            atom_bin (list[int]): Discretized coordinates

        Returns:
            list[float]: Atom 3D coordinates and element

        """

        # Unpack discretized coordinates
        r_bin, t_bin, p_bin = atom_bin

        # Place in the center of radial, polar, azimuthal bin
        r = undiscretize(r_bin, self.radius, 0., self.n_bins)
        t = undiscretize(t_bin, np.pi, 0., self.n_bins)
        p = undiscretize(p_bin, 2. * np.pi, np.pi, self.n_bins)

        return [r, t, p]


class ElementCartesianUniformDiscretizer(CartesianUniformDiscretizer):
    """Discretizer for Cartesian 3D coordinates and element on a uniform grid."""

    coord_type = 'cartesian'
    has_elements = True
    grid_type = 'uniform'
    n_dims = 4

    def discretize(self, atom_3d: list[float]) -> list[int]:
        """Discretize Cartesian 3D coordinates and element on a uniform grid.

        Args:
            atom_3d (list[float]): Atom 3D coordinates and element

        Returns:
            atom_bin (list[int]): Discretized coordinates

        """

        # Unpack Cartesian coordinates and element
        e, x, y, z = atom_3d

        # Compute bin numbers for each coordinate
        e_bin = e
        x_bin, y_bin, z_bin = super().discretize([x, y, z])

        return [e_bin, x_bin, y_bin, z_bin]

    def undiscretize(self, atom_bin: list[int]) -> list[float]:
        """ Undiscretize Cartesian 3D coordinates and element on a uniform grid.

        Args:
            atom_bin (list[int]): Discretized coordinates

        Returns:
            list[float]: Atom 3D coordinates and element

        """

        # Unpack discretized coordinates
        e_bin, x_bin, y_bin, z_bin = atom_bin

        # Place in the center of x, y, z bin
        e = e_bin
        x, y, z = super().undiscretize([x_bin, y_bin, z_bin])

        return [e, x, y, z]


class ElementSphericalUniformDiscretizer(SphericalUniformDiscretizer):
    """Discretizer for spherical polar 3D coordinates and element on a uniform grid."""

    coord_type = 'spherical'
    has_elements = True
    grid_type = 'uniform'
    n_dims = 4

    def discretize(self, atom_3d: list[float]) -> list[int]:
        """Discretize spherical 3D coordinates and element on a uniform grid.

        Args:
            atom_3d (list[float]): Atom 3D coordinates and element

        Returns:
            list[int]: Discretized coordinates

        """

        # Unpack spherical polar coordinates and element
        e, r, t, p = atom_3d

        # Compute bin numbers for each coordinate
        e_bin = e
        r_bin, t_bin, p_bin = super().discretize([r, t, p])

        return [e_bin, r_bin, t_bin, p_bin]

    def undiscretize(self, atom_bin: list[int]) -> list[float]:
        """Undiscretize spherical 3D coordinates and element on a uniform grid.

       Args:
            atom_bin (list[int]): Discretized coordinates

        Returns:
            list[float]: Atom 3D coordinates and element

        """

        # Unpack discretized coordinates
        e_bin, r_bin, t_bin, p_bin = atom_bin

        # Place in the center of radial, polar, azimuthal bin
        e = e_bin
        r, t, p = super().undiscretize([r_bin, t_bin, p_bin])

        return [e, r, t, p]
