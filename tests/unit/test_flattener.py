from molz import Flattener
from molz.flattener import MortonFlattener, HilbertFlattener


def test_factory_default():
    flat = Flattener()
    assert type(flat) is MortonFlattener


def test_factory_hilbert():
    flat = Flattener(coding_type='hilbert')
    assert type(flat) is HilbertFlattener


def test_morton_flatten():
    flat = Flattener(coding_type='morton')
    assert flat.flatten([0, 0, 0, 0]) == 0
    assert flat.flatten([128, 127, 127, 127]) == 2272753527
    assert flat.flatten([255, 255, 255, 255]) == 4294967295


def test_morton_flatten_16bit():
    flat = Flattener(coding_type='morton', n_bits=16, n_dims=2)
    assert flat.flatten([0, 0]) == 0
    assert flat.flatten([4, 128]) == 16416
    assert flat.flatten([255, 255]) == 65535


def test_morton_unflatten():
    flat = Flattener(coding_type='morton')
    assert flat.unflatten(0) == [0, 0, 0, 0]
    assert flat.unflatten(2272753527) == [128, 127, 127, 127]
    assert flat.unflatten(4294967295) == [255, 255, 255, 255]


def test_morton_unflatten_16bit():
    flat = Flattener(coding_type='morton', n_bits=16, n_dims=2)
    assert flat.unflatten(0) == [0, 0]
    assert flat.unflatten(16416) == [4, 128]
    assert flat.unflatten(65535) == [255, 255]


def test_hilbert_flatten():
    flat = Flattener(coding_type='hilbert', n_bits=16, n_dims=4)
    assert flat.flatten([0, 0, 0, 0]) == 0
    assert flat.flatten([128, 127, 127, 127]) == 4116010325
    assert flat.flatten([255, 255, 255, 255]) == 2863311530


def test_hilbert_flatten_16bit():
    flat = Flattener(coding_type='hilbert', n_bits=16, n_dims=2)
    assert flat.flatten([0, 0]) == 0
    assert flat.flatten([4, 128]) == 16442
    assert flat.flatten([255, 255]) == 43690


def test_hilbert_unflatten():
    flat = Flattener(coding_type='hilbert')
    assert flat.unflatten(0) == [0, 0, 0, 0]
    assert flat.unflatten(4116010325) == [128, 127, 127, 127]
    assert flat.unflatten(2863311530) == [255, 255, 255, 255]


def test_hilbert_unflatten_16bit():
    flat = Flattener(coding_type='hilbert', n_bits=16, n_dims=2)
    assert flat.unflatten(0) == [0, 0]
    assert flat.unflatten(16442) == [4, 128]
    assert flat.unflatten(43690) == [255, 255]
