from molz import Molecule3D, MoleculeZ
from molz.molecule_3d import ElementSpherical3D
from pytest import approx


def test_factory_default():
    mol_3d = Molecule3D([[8, 1.30728, 1.61206, 0.09041],
                         [8, 1.30728, 1.61206, -3.05118],
                         [1, 2.42115, 1.20946, -0.62585],
                         [1, 2.42113, 1.20945, 2.51574]])
    assert type(mol_3d) is ElementSpherical3D
    assert mol_3d.coord_type == 'spherical'
    assert mol_3d.has_elements is True
    assert mol_3d.coord == [approx([8, 1.30728, 1.61206, 0.09041]),
                            approx([8, 1.30728, 1.61206, -3.05118]),
                            approx([1, 2.42115, 1.20946, -0.62585]),
                            approx([1, 2.42113, 1.20945, 2.51574])]


def test_from_xyz_default():
    mol_3d = Molecule3D.from_xyz('''4

O    -0.69064    0.02750    0.24251
O     0.69064   -0.02750    0.24251
H    -0.93456   -0.75030   -0.23896
H     0.93456    0.75030   -0.23896
''')
    assert type(mol_3d) is ElementSpherical3D
    assert mol_3d.coord_type == 'spherical'
    assert mol_3d.has_elements is True
    assert mol_3d.coord == [approx([8, 1.3072672, 1.52953671, 3.05117694]),
                            approx([8, 1.3072672, 1.52953671, -0.09041571]),
                            approx([1, 2.4211391, 1.93212921, -2.51574240]),
                            approx([1, 2.4211391, 1.93212921, 0.625850258])]


def test_from_xyz_no_inertial():
    mol_3d = Molecule3D.from_xyz('''4

O    -0.69064    0.02750    0.24251
O     0.69064   -0.02750    0.24251
H    -0.93456   -0.75030   -0.23896
H     0.93456    0.75030   -0.23896
''', to_inertial_frame=False)
    assert mol_3d.coord_type == 'spherical'
    assert mol_3d.has_elements is True
    assert mol_3d.coord == [approx([8, 1.38421756, 1.23335553, 3.10179554]),
                            approx([8, 1.38421756, 1.23335553, -0.03979712]),
                            approx([1, 2.30937705, 1.76760145, -2.46512380]),
                            approx([1, 2.30937705, 1.76760145, 0.676468853])]


def test_from_xyz_no_elements():
    mol_3d = Molecule3D.from_xyz('''4

    O    -0.69064    0.02750    0.24251
    O     0.69064   -0.02750    0.24251
    H    -0.93456   -0.75030   -0.23896
    H     0.93456    0.75030   -0.23896
    ''', has_elements=False)
    assert mol_3d.coord_type == 'spherical'
    assert mol_3d.has_elements is False
    assert mol_3d.coord == [approx([1.3072672, 1.52953671, 3.05117694]),
                            approx([1.3072672, 1.52953671, -0.09041571]),
                            approx([2.4211391, 1.93212921, -2.51574240]),
                            approx([2.4211391, 1.93212921, 0.625850258])]
    assert mol_3d.elements == [8, 8, 1, 1]


def test_from_xyz_cartesian():
    mol_3d = Molecule3D.from_xyz('''4

    O    -0.69064    0.02750    0.24251
    O     0.69064   -0.02750    0.24251
    H    -0.93456   -0.75030   -0.23896
    H     0.93456    0.75030   -0.23896
    ''', coord_type='cartesian')
    assert mol_3d.coord_type == 'cartesian'
    assert mol_3d.has_elements is True
    assert mol_3d.coord == [approx([8, -1.30081939, 0.11793606, 0.05392205]),
                            approx([8, 1.30081939, -0.11793606, 0.05392205]),
                            approx([1, -1.8355399, -1.32668802, -0.85592439]),
                            approx([1, 1.8355399, 1.32668802, -0.85592439])]


def test_from_xyz_cartesian_no_elements():
    mol_3d = Molecule3D.from_xyz('''4

    O    -0.69064    0.02750    0.24251
    O     0.69064   -0.02750    0.24251
    H    -0.93456   -0.75030   -0.23896
    H     0.93456    0.75030   -0.23896
    ''', coord_type='cartesian', has_elements=False)
    assert mol_3d.coord_type == 'cartesian'
    assert mol_3d.has_elements is False
    assert mol_3d.coord == [approx([-1.30081939, 0.11793606, 0.05392205]),
                            approx([1.30081939, -0.11793606, 0.05392205]),
                            approx([-1.8355399, -1.32668802, -0.85592439]),
                            approx([1.8355399, 1.32668802, -0.85592439])]
    assert mol_3d.elements == [8, 8, 1, 1]


def test_cartesian_to_xyz():
    mol_3d = Molecule3D([[8, -1.30081939, 0.11793606, 0.05392205],
                         [8, 1.30081939, -0.11793606, 0.05392205],
                         [1, -1.8355399, -1.32668802, -0.85592439],
                         [1, 1.8355399, 1.32668802, -0.85592439]],
                        coord_type='cartesian', has_elements=False)
    xyz = mol_3d.to_xyz()
    assert xyz == '''4

O        -0.68836        0.06241        0.02853
O         0.68836       -0.06241        0.02853
H        -0.97133       -0.70205       -0.45294
H         0.97133        0.70205       -0.45294
'''


def test_spherical_to_xyz():
    mol_3d = Molecule3D([[8, 1.30728, 1.61206, 0.09041],
                         [8, 1.30728, 1.61206, -3.05118],
                         [1, 2.42115, 1.20946, -0.62585],
                         [1, 2.42113, 1.20945, 2.51574]],
                        coord_type='spherical', has_elements=False)
    xyz = mol_3d.to_xyz()
    assert xyz == '''4

O         0.68837        0.06241       -0.02854
O        -0.68837       -0.06241       -0.02854
H         0.97133       -0.70206        0.45294
H        -0.97132        0.70205        0.45295
'''


def test_element_cartesian_to_xyz():
    mol_3d = Molecule3D([[8, -1.30081939, 0.11793606, 0.05392205],
                         [8, 1.30081939, -0.11793606, 0.05392205],
                         [1, -1.8355399, -1.32668802, -0.85592439],
                         [1, 1.8355399, 1.32668802, -0.85592439]],
                        coord_type='cartesian')
    xyz = mol_3d.to_xyz()
    assert xyz == '''4

O        -0.68836        0.06241        0.02853
O         0.68836       -0.06241        0.02853
H        -0.97133       -0.70205       -0.45294
H         0.97133        0.70205       -0.45294
'''


def test_element_spherical_to_xyz():
    mol_3d = Molecule3D([[8, 1.30728, 1.61206, 0.09041],
                         [8, 1.30728, 1.61206, -3.05118],
                         [1, 2.42115, 1.20946, -0.62585],
                         [1, 2.42113, 1.20945, 2.51574]],
                        coord_type='spherical')
    xyz = mol_3d.to_xyz()
    assert xyz == '''4

O         0.68837        0.06241       -0.02854
O        -0.68837       -0.06241       -0.02854
H         0.97133       -0.70206        0.45294
H        -0.97132        0.70205        0.45295
'''


def test_encode_default():
    mol_3d = Molecule3D.from_xyz('''4

    O    -0.69064    0.02750    0.24251
    O     0.69064   -0.02750    0.24251
    H    -0.93456   -0.75030   -0.23896
    H     0.93456    0.75030   -0.23896
    ''')
    mol_z = mol_3d.encode()
    assert mol_z.coord_type == 'spherical'
    assert mol_z.has_elements is True
    assert mol_z.grid_type == 'uniform'
    assert mol_z.coding_type == 'morton'
    assert mol_z.n_bits == 8
    assert mol_z.n_dims == 4
    assert mol_z.coord == [326349572, 57914116, 541554191, 809989647]
    assert mol_z.elements is None


def test_encode_cartesian():
    mol_3d = Molecule3D.from_xyz('''4

    O    -0.69064    0.02750    0.24251
    O     0.69064   -0.02750    0.24251
    H    -0.93456   -0.75030   -0.23896
    H     0.93456    0.75030   -0.23896
    ''', coord_type='cartesian')
    mol_z = mol_3d.encode()
    assert mol_z.coord_type == 'cartesian'
    assert mol_z.has_elements is True
    assert mol_z.grid_type == 'uniform'
    assert mol_z.coding_type == 'morton'
    assert mol_z.n_bits == 8
    assert mol_z.n_dims == 4
    assert mol_z.coord == [872727651, 1382195717, 118895164, 1634751578]
    assert mol_z.elements is None


def test_encode_hilbert():
    mol_3d = Molecule3D.from_xyz('''4

    O    -0.69064    0.02750    0.24251
    O     0.69064   -0.02750    0.24251
    H    -0.93456   -0.75030   -0.23896
    H     0.93456    0.75030   -0.23896
    ''')
    mol_z = mol_3d.encode(coding_type='hilbert')
    assert mol_z.coord_type == 'spherical'
    assert mol_z.has_elements is True
    assert mol_z.grid_type == 'uniform'
    assert mol_z.coding_type == 'hilbert'
    assert mol_z.n_bits == 8
    assert mol_z.n_dims == 4
    assert mol_z.coord == [504797827, 236362371, 1056684042, 544715786]
    assert mol_z.elements is None


def test_encode_hilbert_cartesian():
    mol_3d = Molecule3D.from_xyz('''4

    O    -0.69064    0.02750    0.24251
    O     0.69064   -0.02750    0.24251
    H    -0.93456   -0.75030   -0.23896
    H     0.93456    0.75030   -0.23896
    ''', coord_type='cartesian')
    mol_z = mol_3d.encode(coding_type='hilbert')
    assert mol_z.coord_type == 'cartesian'
    assert mol_z.has_elements is True
    assert mol_z.grid_type == 'uniform'
    assert mol_z.coding_type == 'hilbert'
    assert mol_z.n_bits == 8
    assert mol_z.n_dims == 4
    assert mol_z.coord == [797124008, 1674436870, 233963786, 1339059496]
    assert mol_z.elements is None


def test_encode_no_elements():
    mol_3d = Molecule3D.from_xyz('''4

    O    -0.69064    0.02750    0.24251
    O     0.69064   -0.02750    0.24251
    H    -0.93456   -0.75030   -0.23896
    H     0.93456    0.75030   -0.23896
    ''', has_elements=False)
    mol_z = mol_3d.encode()
    assert mol_z.coord_type == 'spherical'
    assert mol_z.has_elements is False
    assert mol_z.grid_type == 'uniform'
    assert mol_z.coding_type == 'morton'
    assert mol_z.n_bits == 8
    assert mol_z.n_dims == 3
    assert mol_z.coord == [3126980, 1029828, 4358023, 6455175]
    assert mol_z.elements == [8, 8, 1, 1]


def test_encode_cartesian_no_elements():
    mol_3d = Molecule3D.from_xyz('''4

    O    -0.69064    0.02750    0.24251
    O     0.69064   -0.02750    0.24251
    H    -0.93456   -0.75030   -0.23896
    H     0.93456    0.75030   -0.23896
    ''', coord_type='cartesian', has_elements=False)
    mol_z = mol_3d.encode()
    assert mol_z.coord_type == 'cartesian'
    assert mol_z.has_elements is False
    assert mol_z.grid_type == 'uniform'
    assert mol_z.coding_type == 'morton'
    assert mol_z.n_bits == 8
    assert mol_z.n_dims == 3
    assert mol_z.coord == [7358771, 11216005, 1894044, 13077290]
    assert mol_z.elements == [8, 8, 1, 1]


def test_encode_hilbert_no_elements():
    mol_3d = Molecule3D.from_xyz('''4

    O    -0.69064    0.02750    0.24251
    O     0.69064   -0.02750    0.24251
    H    -0.93456   -0.75030   -0.23896
    H     0.93456    0.75030   -0.23896
    ''', has_elements=False)
    mol_z = mol_3d.encode(coding_type='hilbert')
    assert mol_z.coord_type == 'spherical'
    assert mol_z.has_elements is False
    assert mol_z.grid_type == 'uniform'
    assert mol_z.coding_type == 'hilbert'
    assert mol_z.n_bits == 8
    assert mol_z.n_dims == 3
    assert mol_z.coord == [3712161, 1615009, 8116997, 4307589]
    assert mol_z.elements == [8, 8, 1, 1]


def test_encode_hilbert_cartesian_no_elements():
    mol_3d = Molecule3D.from_xyz('''4

    O    -0.69064    0.02750    0.24251
    O     0.69064   -0.02750    0.24251
    H    -0.93456   -0.75030   -0.23896
    H     0.93456    0.75030   -0.23896
    ''', coord_type='cartesian', has_elements=False)
    mol_z = mol_3d.encode(coding_type='hilbert')
    assert mol_z.coord_type == 'cartesian'
    assert mol_z.has_elements is False
    assert mol_z.grid_type == 'uniform'
    assert mol_z.coding_type == 'hilbert'
    assert mol_z.n_bits == 8
    assert mol_z.n_dims == 3
    assert mol_z.coord == [6165076, 13570626, 1548357, 10273255]
    assert mol_z.elements == [8, 8, 1, 1]


def test_decode_default():
    mol_z = MoleculeZ([326349572, 57914116, 541554191, 809989647])
    mol_3d = Molecule3D.decode(mol_z)
    assert mol_3d.coord_type == 'spherical'
    assert mol_3d.has_elements is True
    assert mol_3d.n_dims == 4
    assert mol_3d.coord == [
        approx([8, 1.30859375, 1.52784486, 3.05568973]),
        approx([8, 1.30859375, 1.52784486, -0.08590292]),
        approx([1, 2.40234375, 1.93281579, -2.51572849]),
        approx([1, 2.40234375, 1.93281579, 0.62586416])
    ]
    assert mol_3d.elements is None


def test_decode_cartesian():
    mol_z = MoleculeZ([872727651, 1382195717, 118895164, 1634751578],
                      coord_type='cartesian')
    mol_3d = Molecule3D(coord_type='cartesian').decode(mol_z)
    assert mol_3d.coord_type == 'cartesian'
    assert mol_3d.has_elements is True
    assert mol_3d.n_dims == 4
    assert mol_3d.coord == [
        approx([8, -1.30859375, 0.13671875, 0.05859375]),
        approx([8, 1.30859375, -0.13671875, 0.05859375]),
        approx([1, -1.81640625, -1.30859375, -0.83984375]),
        approx([1, 1.81640625, 1.30859375, -0.83984375])
    ]
    assert mol_3d.elements is None


def test_decode_hilbert():
    mol_z = MoleculeZ([504797827, 236362371, 1056684042, 544715786],
                      coding_type='hilbert')
    mol_3d = Molecule3D().decode(mol_z, coding_type='hilbert')
    assert mol_3d.coord_type == 'spherical'
    assert mol_3d.has_elements is True
    assert mol_3d.n_dims == 4
    assert mol_3d.coord == [
        approx([8, 1.30859375, 1.52784486, 3.05568973]),
        approx([8, 1.30859375, 1.52784486, -0.08590292]),
        approx([1, 2.40234375, 1.93281579, -2.51572849]),
        approx([1, 2.40234375, 1.93281579, 0.62586416])
    ]
    assert mol_3d.elements is None


def test_decode_hilbert_cartesian():
    mol_z = MoleculeZ([797124008, 1674436870, 233963786, 1339059496],
                      coord_type='cartesian', coding_type='hilbert')
    mol_3d = Molecule3D(coord_type='cartesian').decode(mol_z, coding_type='hilbert')
    assert mol_3d.coord_type == 'cartesian'
    assert mol_3d.has_elements is True
    assert mol_3d.n_dims == 4
    assert mol_3d.coord == [
        approx([8, -1.30859375, 0.13671875, 0.05859375]),
        approx([8, 1.30859375, -0.13671875, 0.05859375]),
        approx([1, -1.81640625, -1.30859375, -0.83984375]),
        approx([1, 1.81640625, 1.30859375, -0.83984375])
    ]
    assert mol_3d.elements is None


def test_decode_no_elements():
    mol_z = MoleculeZ([3126980, 1029828, 4358023, 6455175], [8, 8, 1, 1],
                      has_elements=False)
    mol_3d = Molecule3D.decode(mol_z)
    assert mol_3d.coord_type == 'spherical'
    assert mol_3d.has_elements is False
    assert mol_3d.n_dims == 3
    assert mol_3d.coord == [
        approx([1.30859375, 1.52784486, 3.05568973]),
        approx([1.30859375, 1.52784486, -0.08590292]),
        approx([2.40234375, 1.93281579, -2.51572849]),
        approx([2.40234375, 1.93281579, 0.62586416])
    ]
    assert mol_3d.elements == [8, 8, 1, 1]


def test_decode_cartesian_no_elements():
    mol_z = MoleculeZ([7358771, 11216005, 1894044, 13077290], [8, 8, 1, 1],
                      coord_type='cartesian', has_elements=False)
    mol_3d = Molecule3D.decode(mol_z)
    assert mol_3d.coord_type == 'cartesian'
    assert mol_3d.has_elements is False
    assert mol_3d.n_dims == 3
    assert mol_3d.coord == [
        approx([-1.30859375, 0.13671875, 0.05859375]),
        approx([1.30859375, -0.13671875, 0.05859375]),
        approx([-1.81640625, -1.30859375, -0.83984375]),
        approx([1.81640625, 1.30859375, -0.83984375])
    ]
    assert mol_3d.elements == [8, 8, 1, 1]


def test_decode_hilbert_no_elements():
    mol_z = MoleculeZ([3712161, 1615009, 8116997, 4307589], [8, 8, 1, 1],
                      has_elements=False, coding_type='hilbert')
    mol_3d = Molecule3D.decode(mol_z, coding_type='hilbert')
    assert mol_3d.coord_type == 'spherical'
    assert mol_3d.has_elements is False
    assert mol_3d.n_dims == 3
    assert mol_3d.coord == [
        approx([1.30859375, 1.52784486, 3.05568973]),
        approx([1.30859375, 1.52784486, -0.08590292]),
        approx([2.40234375, 1.93281579, -2.51572849]),
        approx([2.40234375, 1.93281579, 0.62586416])
    ]
    assert mol_3d.elements == [8, 8, 1, 1]


def test_decode_hilbert_cartesian_no_elements():
    mol_z = MoleculeZ([6165076, 13570626, 1548357, 10273255], [8, 8, 1, 1],
                      coord_type='cartesian', has_elements=False, coding_type='hilbert')
    mol_3d = Molecule3D.decode(mol_z, coding_type='hilbert')
    assert mol_3d.coord_type == 'cartesian'
    assert mol_3d.has_elements is False
    assert mol_3d.n_dims == 3
    assert mol_3d.coord == [
        approx([-1.30859375, 0.13671875, 0.05859375]),
        approx([1.30859375, -0.13671875, 0.05859375]),
        approx([-1.81640625, -1.30859375, -0.83984375]),
        approx([1.81640625, 1.30859375, -0.83984375])
    ]
    assert mol_3d.elements == [8, 8, 1, 1]
