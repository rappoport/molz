import pytest
from molz.utils import parse_xyz, transpose, xyz2cart, cart2xyz, \
    format_xyz, inert_frame, cart2sph, sph2cart, get_min_max,\
    transform_3d, split_elements, combine_elements,\
    discretize, undiscretize, interleave, deinterleave,\
    hilbert_transform, hilbert_restore, soi_rotations
from pytest import approx


def test_xyz2coord():
    coord = xyz2cart('''5

C          0.89279        0.00106       -0.02167
F          2.25095        0.00106       -0.02167
H          0.54274        0.91863       -0.49789
H          0.54274       -0.04530        1.01108
H          0.54274       -0.87015       -0.57819''')

    assert coord == [approx([6, 1.68712859, 0.002003110, -0.04095037]),
                     approx([9, 4.25367902, 0.002003110, -0.04095037]),
                     approx([1, 1.02562996, 1.735959110, -0.94087574]),
                     approx([1, 1.02562996, -0.08560459, 1.910664290]),
                     approx([1, 1.02562996, -1.64434519, -1.092620748])]


def test_cart2xyz():
    xyz = cart2xyz([[8, 1.99855545, -0.11729530, 0.11105920],
                    [17, 5.16012395, -0.126082527, 0.10586246],
                    [1, 1.36353189, 1.36876643, 0.98923383]])
    assert xyz == '''3

O         1.05759       -0.06207        0.05877
Cl        2.73062       -0.06672        0.05602
H         0.72155        0.72432        0.52348
'''


def test_parse_xyz():
    el, x, y, z = parse_xyz('''3

O          1.05759       -0.06207        0.05877
Cl         2.73062       -0.06672        0.05602
H          0.72155        0.72432        0.52348
''')
    assert el.tolist() == [8, 17, 1]
    assert x.tolist() == approx([1.99855545, 5.16012395, 1.36353189])
    assert y.tolist() == approx([-0.11729530, -0.126082527, 1.36876643])
    assert z.tolist() == approx([0.11105920, 0.10586246, 0.98923383])


def test_transpose():
    coord = transpose([35, 9, 9, 9],
                      [0.46407, 2.87744, 3.37308, 3.37308],
                      [1.57080, 1.57080, 1.57080, 1.57080],
                      [4.71239, 1.57080, 3.27924, 6.14554])
    assert coord == [[35, 0.46407, 1.5708, 4.71239], [9, 2.87744, 1.5708, 1.5708],
                     [9, 3.37308, 1.5708, 3.27924], [9, 3.37308, 1.5708, 6.14554]]


def test_format_xyz():
    assert format_xyz([9, 9], [-1.2623, 1.2623], [0., 0.], [-0., -0.]) == '''2

F        -0.66798        0.00000       -0.00000
F         0.66798        0.00000       -0.00000
'''


def test_inert_frame():
    x, y, z = inert_frame([8, 17, 1], [1.99856, -0.11730, 0.11106],
                          [5.16012, -0.12608, 0.10586], [1.36353, 1.36877, 0.98923])
    assert x.tolist() == approx([3.95161092, -1.74230836, -1.44241526])
    assert y.tolist() == approx([0.00129266, 0.01049265, -0.38958133])
    assert z.tolist() == approx([0., 0., 0.], abs=1e-6)


def test_inert_frame_idem():
    x, y, z = inert_frame([8, 17, 1], [1.99856, -0.11730, 0.11106],
                          [5.16012, -0.12608, 0.10586], [1.36353, 1.36877, 0.98923])
    assert x.tolist() == approx([3.95161092, -1.74230836, -1.44241526])
    assert y.tolist() == approx([0.00129266, 0.01049265, -0.38958133])
    assert z.tolist() == approx([0., 0., 0.], abs=1e-6)


def test_cart2sph():
    rad, theta, phi = cart2sph([-1.12243, 0.54916, -1.49911],
                               [-0.06431, 0.00534, 0.83313],
                               [0., 0., 0.])
    assert rad.tolist() == approx([1.12427082, 0.54918596, 1.71506163])
    assert theta.tolist() == approx([1.57079633, 1.57079633, 1.57079633])
    assert phi.tolist() == approx([-3.08435989, 0.00972364, 2.63434577])


def test_sph2cart():
    x, y, z = sph2cart([1.2623, 1.2623], [1.5708, 1.5708], [3.14159, 0.])
    assert x.tolist() == approx([-1.2623, 1.2623])
    assert y.tolist() == approx([0., 0.], abs=1e-5)
    assert z.tolist() == approx([0., 0.], abs=1e-5)


def test_get_min_max():
    x_min, x_max, y_min, y_max, z_min, z_max = get_min_max(
        [[0.46407, 1.5708, 4.71239], [2.87744, 1.5708, 1.5708],
         [3.37308, 1.5708, 3.27924], [3.37308, 1.5708, 6.14554]])
    assert x_min == approx(0.46407)
    assert x_max == approx(3.37308)
    assert y_min == approx(1.5708)
    assert y_max == approx(1.5708)
    assert z_min == approx(1.5708)
    assert z_max == approx(6.14554)


def test_transform_3d_cartesian_cartesian():
    coord = transform_3d([[8, 1.99855545, -0.11729530, 0.11105920],
                          [17, 5.16012395, -0.126082527, 0.10586246],
                          [1, 1.36353189, 1.36876643, 0.98923383]],
                         input_type='cartesian', coord_type='cartesian')
    assert coord == [approx([8, 1.99855545, -0.11729530, 0.11105920]),
                     approx([17, 5.16012395, -0.126082527, 0.10586246]),
                     approx([1, 1.36353189, 1.36876643, 0.98923383])]


def test_transform_3d_cartesian_cartesian_inertial():
    coord = transform_3d([[8, 1.99855545, -0.11729530, 0.11105920],
                          [17, 5.16012395, -0.126082527, 0.10586246],
                          [1, 1.36353189, 1.36876643, 0.98923383]],
                         input_type='cartesian', coord_type='cartesian',
                         to_inertial_frame=True)
    assert coord == [approx([8, -2.12108270, -0.12153429, 0.0], abs=1e-6),
                     approx([17, 1.03776132, 0.01008638, 0.0], abs=1e-6),
                     approx([1, -2.83289710, 1.57438591, 0.0], abs=1e-6)]


def test_transform_3d_cartesian_spherical():
    coord = transform_3d([[8, 1.99855545, -0.11729530, 0.11105920],
                          [17, 5.16012395, -0.126082527, 0.10586246],
                          [1, 1.36353189, 1.36876643, 0.98923383]],
                         input_type='cartesian', coord_type='spherical')

    assert coord == [approx([8, 2.0050726, 1.51537885, -0.05862279]),
                     approx([17, 5.16274954, 1.55028984, -0.02442915]),
                     approx([1, 2.17055853, 1.09758078, 0.78731396])]


def test_transform_3d_cartesian_spherical_inertial():
    coord = transform_3d([[8, 1.99855545, -0.11729530, 0.11105920],
                          [17, 5.16012395, -0.126082527, 0.10586246],
                          [1, 1.36353189, 1.36876643, 0.98923383]],
                         input_type='cartesian', coord_type='spherical',
                         to_inertial_frame=True)
    assert coord == [approx([8, 2.1245617, 1.57079633, -3.08435700]),
                     approx([17, 1.0378103, 1.57079633, 0.0097190538]),
                     approx([1, 3.24098703, 1.57079633, 2.6343446736])]


def test_transform_3d_spherical_cartesian():
    coord = transform_3d([[8, 2.0050726, 1.51537885, -0.05862279],
                          [17, 5.16274954, 1.55028984, -0.02442915],
                          [1, 2.17055853, 1.09758078, 0.78731396]],
                         input_type='spherical', coord_type='cartesian')
    assert coord == [approx([8, 1.99855545, -0.11729530, 0.11105920]),
                     approx([17, 5.16012395, -0.126082527, 0.10586246]),
                     approx([1, 1.36353189, 1.36876643, 0.98923383])]


def test_transform_3d_spherical_cartesian_inertial():
    coord = transform_3d([[8, 2.0050726, 1.51537885, -0.05862279],
                          [17, 5.16274954, 1.55028984, -0.02442915],
                          [1, 2.17055853, 1.09758078, 0.78731396]],
                         input_type='spherical', coord_type='cartesian',
                         to_inertial_frame=True)
    assert coord == [approx([8, -2.12108270, -0.12153429, 0.0], abs=1e-6),
                     approx([17, 1.03776132, 0.01008638, 0.0], abs=1e-6),
                     approx([1, -2.83289710, 1.57438591, 0.0], abs=1e-6)]


def test_transform_3d_spherical_spherical():
    coord = transform_3d([[8, 2.0050726, 1.51537885, -0.05862279],
                          [17, 5.16274954, 1.55028984, -0.02442915],
                          [1, 2.17055853, 1.09758078, 0.78731396]],
                         input_type='spherical', coord_type='spherical')
    assert coord == [approx([8, 2.0050726, 1.51537885, -0.05862279]),
                     approx([17, 5.16274954, 1.55028984, -0.02442915]),
                     approx([1, 2.17055853, 1.09758078, 0.78731396])]


def test_transform_3d_spherical_spherical_inertial():
    coord = transform_3d([[8, 2.0050726, 1.51537885, -0.05862279],
                          [17, 5.16274954, 1.55028984, -0.02442915],
                          [1, 2.17055853, 1.09758078, 0.78731396]],
                         input_type='spherical', coord_type='spherical',
                         to_inertial_frame=True)

    assert coord == [approx([8, 2.1245617, 1.57079633, -3.08435700]),
                     approx([17, 1.0378103, 1.57079633, 0.0097190538]),
                     approx([1, 3.24098703, 1.57079633, 2.6343446736])]


def test_split_elements():
    assert split_elements([[8, 2.0050726, 1.51537885, -0.05862279],
                           [17, 5.16274954, 1.55028984, -0.02442915],
                           [1, 2.17055853, 1.09758078, 0.78731396]]) == (
               [8, 17, 1],
               [approx([2.0050726, 1.51537885, -0.05862279]),
                approx([5.16274954, 1.55028984, -0.02442915]),
                approx([2.17055853, 1.09758078, 0.78731396])]
           )


def test_combine_elements():
    assert combine_elements([8, 17, 1],
                            [[2.0050726, 1.51537885, -0.05862279],
                             [5.16274954, 1.55028984, -0.02442915],
                             [2.17055853, 1.09758078, 0.78731396]]) == [
               [8, 2.0050726, 1.51537885, -0.05862279],
               [17, 5.16274954, 1.55028984, -0.02442915],
               [1, 2.17055853, 1.09758078, 0.78731396]]


def test_discretize():
    assert discretize(0., 10., 5., 256) == 128
    assert discretize(1., 10., 5., 256) == 153


def test_undiscretize():
    assert undiscretize(128, 10., 5., 256) == approx(0.01953125)
    assert undiscretize(153, 10., 5., 256) == approx(0.99609375)


def test_interleave():
    assert interleave([128, 127, 127, 127], 8, 4) == 2272753527
    assert interleave([128, 255, 128, 255], 8, 4) == 4116010325


def test_deinterleave():
    assert deinterleave(2272753527, 8, 4) == [128, 127, 127, 127]
    assert deinterleave(4116010325, 8, 4) == [128, 255, 128, 255]


def test_hilbert_transform():
    assert hilbert_transform([128, 127, 127, 127], 8, 4) == [128, 255, 128, 255]


def test_hilbert_restore():
    assert hilbert_restore([128, 255, 128, 255], 8, 4) == [128, 127, 127, 127]


def test_soi_rotations():
    rotations = list(soi_rotations(1))
    assert len(rotations) == 72
    assert rotations[0].as_matrix().tolist() == [approx([0.80502117, 0.2723291, 0.52704628]),
                                                 approx([0.56100423, -0.6383545, -0.52704628]),
                                                 approx([0.19291233, 0.7199586, -0.66666667])]


def test_soi_rotations_invalid():
    with pytest.raises(ValueError, match='Invalid resolution parameter in soi_rotations'):
        next(soi_rotations(0))
