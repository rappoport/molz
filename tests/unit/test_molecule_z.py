import pytest
from molz import MoleculeZ, Molecule3D
from molz.molecule_z import CartesianUniformMortonZ, SphericalUniformMortonZ, \
    ElementSphericalUniformMortonZ, ElementCartesianUniformMortonZ, \
    ElementSphericalUniformHilbertZ
from pytest import approx


def test_factory_default():
    mol_z = MoleculeZ([2193670336, 3502293168], n_bits=8)
    assert type(mol_z) is ElementSphericalUniformMortonZ
    assert mol_z.coord_type == 'spherical'
    assert mol_z.has_elements is True
    assert mol_z.grid_type == 'uniform'
    assert mol_z.coding_type == 'morton'
    assert mol_z.coord == [2193670336, 3502293168]
    assert mol_z.elements is None


def test_factory_hilbert():
    mol_z = MoleculeZ([4026501489, 4293919549], coding_type='hilbert', n_bits=8)
    assert type(mol_z) is ElementSphericalUniformHilbertZ
    assert mol_z.coord_type == 'spherical'
    assert mol_z.has_elements is True
    assert mol_z.grid_type == 'uniform'
    assert mol_z.coding_type == 'hilbert'
    assert mol_z.coord == [4026501489, 4293919549]
    assert mol_z.elements is None


def test_factory_no_elements():
    mol_z = MoleculeZ([1024, 4218880], [8, 1], has_elements=False, n_bits=8)
    assert type(mol_z) is SphericalUniformMortonZ
    assert mol_z.coord_type == 'spherical'
    assert mol_z.has_elements is False
    assert mol_z.grid_type == 'uniform'
    assert mol_z.coding_type == 'morton'
    assert mol_z.coord == [1024, 4218880]
    assert mol_z.elements == [8, 1]


def test_factory_cartesian():
    mol_z = MoleculeZ([2864758976, 3955277955], coord_type='cartesian', n_bits=8)
    assert type(mol_z) is ElementCartesianUniformMortonZ
    assert mol_z.coord_type == 'cartesian'
    assert mol_z.has_elements is True
    assert mol_z.grid_type == 'uniform'
    assert mol_z.coding_type == 'morton'
    assert mol_z.coord == [2864758976, 3955277955]
    assert mol_z.elements is None


def test_factory_cartesian_no_elements():
    mol_z = MoleculeZ([8389664, 8390179], [8, 1], coord_type='cartesian',
                      has_elements=False, n_bits=8)
    assert type(mol_z) is CartesianUniformMortonZ
    assert mol_z.coord_type == 'cartesian'
    assert mol_z.has_elements is False
    assert mol_z.grid_type == 'uniform'
    assert mol_z.coding_type == 'morton'
    assert mol_z.coord == [8389664, 8390179]
    assert mol_z.elements == [8, 1]


def test_factory_elements_error():
    with pytest.raises(ValueError, match='Element identifiers required'):
        MoleculeZ([8389664, 8390179], coord_type='cartesian',
                  has_elements=False, n_bits=8)


def test_factory_no_elements_error():
    with pytest.raises(ValueError, match='Element identifiers ignored'):
        MoleculeZ([8389664, 8390179], [8, 1], coord_type='cartesian',
                  has_elements=True, n_bits=8)


def test_from_json_default():
    mol_z = MoleculeZ.from_json('''{
    "grid_type": "uniform", 
    "n_dims": 4,
    "coding_type": "morton",
    "n_bits": 8, 
    "coord_type": "spherical",
    "coord": [2193670336, 3502293168]
}''')
    assert type(mol_z) is ElementSphericalUniformMortonZ
    assert mol_z.coord_type == 'spherical'
    assert mol_z.has_elements is True
    assert mol_z.grid_type == 'uniform'
    assert mol_z.coding_type == 'morton'
    assert mol_z.coord == [2193670336, 3502293168]
    assert mol_z.elements is None


def test_from_json_no_elements():
    mol_z = MoleculeZ.from_json('''{
    "coord_type": "spherical",
    "has_elements": false,
    "grid_type": "uniform",
    "coding_type": "morton",
    "coord": [1024, 4218880],
    "elements": [8, 1],
    "n_bits": 8,
    "n_dims": 4
    }''')
    assert type(mol_z) is SphericalUniformMortonZ
    assert mol_z.coord_type == 'spherical'
    assert mol_z.has_elements is False
    assert mol_z.grid_type == 'uniform'
    assert mol_z.coding_type == 'morton'
    assert mol_z.coord == [1024, 4218880]
    assert mol_z.elements == [8, 1]


def test_spherical_to_json():
    mol_z = MoleculeZ([1024, 4218880], [8, 1], has_elements=False, n_bits=8)
    assert mol_z.to_json() == '{"coord_type": "spherical", "has_elements": false, "grid_type": "uniform", "coding_type": "morton", "coord": [1024, 4218880], "elements": [8, 1], "n_bits": 8, "n_dims": 3}'


def test_element_spherical_to_json():
    mol_z = MoleculeZ([2193670336, 3502293168], n_bits=8)
    assert mol_z.to_json() == '{"coord_type": "spherical", "has_elements": true, "grid_type": "uniform", "coding_type": "morton", "coord": [2193670336, 3502293168], "elements": null, "n_bits": 8, "n_dims": 4}'


def test_element_cartesian_to_json():
    mol_z = MoleculeZ([2864758976, 3955277955], coord_type='cartesian', n_bits=8)
    assert mol_z.to_json() == '{"coord_type": "cartesian", "has_elements": true, "grid_type": "uniform", "coding_type": "morton", "coord": [2864758976, 3955277955], "elements": null, "n_bits": 8, "n_dims": 4}'


def test_encode_default():
    mol_3d = Molecule3D.from_xyz('''5

S     0.0000000    0.0000000    0.5212994
O     0.0000000   -1.2346393    1.1935291
O     0.0000000    1.2346393    1.1935291
Cl   -1.5480166    0.0000000   -0.7743554
Cl    1.5480166    0.0000000   -0.7743554''')
    mol_z = MoleculeZ.encode(mol_3d)
    assert mol_z.coord_type == 'spherical'
    assert mol_z.has_elements is True
    assert mol_z.coord == [822886404, 923041862, 354853476, 622596429, 604831836]
    assert mol_z.elements is None


def test_encode_no_elements():
    mol_3d = Molecule3D.from_xyz('''5

    S     0.0000000    0.0000000    0.5212994
    O     0.0000000   -1.2346393    1.1935291
    O     0.0000000    1.2346393    1.1935291
    Cl   -1.5480166    0.0000000   -0.7743554
    Cl    1.5480166    0.0000000   -0.7743554''', has_elements=False)
    mol_z = MoleculeZ.encode(mol_3d)
    assert mol_z.coord_type == 'spherical'
    assert mol_z.has_elements is False
    assert mol_z.coord == [6572036, 8142886, 3499188, 5554789, 5263404]
    assert mol_z.elements == [16, 8, 8, 17, 17]


def test_decode_default():
    mol_z = MoleculeZ([822886404, 923041862, 354853476, 622596429, 604831836],
                      coord_type='spherical', has_elements=True, grid_type='uniform',
                      coding_type='morton', n_bits=8)
    mol_3d = mol_z.decode()
    assert mol_3d.coord_type == 'spherical'
    assert mol_3d.has_elements is True
    assert mol_3d.coord == [approx([16, 0.99609375, 1.57693225, 1.58306817]),
                            approx([8, 3.26171875, 2.37460226, 1.58306817]),
                            approx([8, 3.26171875, 0.76699039, 1.58306817]),
                            approx([17, 3.26171875, 1.57693225, -0.45405831]),
                            approx([17, 3.26171875, 1.57693225, -2.68753434])]
    assert mol_3d.elements is None


def test_decode_no_elements():
    mol_z = MoleculeZ([6572036, 8142886, 3499188, 5554789, 5263404], [16, 8, 8, 17, 17],
                      coord_type='spherical', has_elements=False, grid_type='uniform',
                      coding_type='morton', n_bits=8)
    mol_3d = mol_z.decode()
    assert mol_3d.coord_type == 'spherical'
    assert mol_3d.has_elements is False
    assert mol_3d.coord == [approx([0.99609375, 1.57693225, 1.58306817]),
                            approx([3.26171875, 2.37460226, 1.58306817]),
                            approx([3.26171875, 0.76699039, 1.58306817]),
                            approx([3.26171875, 1.57693225, -0.45405831]),
                            approx([3.26171875, 1.57693225, -2.68753434])]
    assert mol_3d.elements == [16, 8, 8, 17, 17]


def test_distance_default():
    mol_3d_cis = Molecule3D.from_xyz('''4

  N       -5.01001        6.31224        0.00000
  N       -3.78999        6.31224       -0.00000
  H       -3.32155        7.12144        0.00000
  H       -5.47845        7.12144       -0.00000
''')
    mol_3d_trans = Molecule3D.from_xyz('''4

  N       -4.95248        5.91810        0.00000
  N       -3.76417        6.19572       -0.00000
  H       -3.49491        7.09000        0.00000
  H       -5.22344        5.02354        0.00000
''')
    mol_z_cis = mol_3d_cis.encode()
    mol_z_trans = mol_3d_trans.encode()
    assert mol_z_cis.distance(mol_z_trans) == 48997248
    assert mol_z_trans.distance(mol_z_cis) == 48997248
