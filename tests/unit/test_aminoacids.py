import pytest
from molz.aminoacids import aminoacid_code


def test_aminoacid_code():
    assert aminoacid_code('GLU', 'pfm', 1) == 348
    assert aminoacid_code('E', 'pfm', 1) == 348
    assert aminoacid_code('GLU', 'lk', 1) == 24
    assert aminoacid_code('GLU', 'lk_3d', 1) == 6
    assert aminoacid_code('GLU', 'zbts', 1) == 224
    assert aminoacid_code('GLU', 'blomap', 1) == 3
    assert aminoacid_code('GLU', 'blomap', 1, 3) == 24
    assert aminoacid_code('GLU', 'blomap_3d', 1) == 0
    with pytest.raises(ValueError, match='Invalid encoding type'):
        aminoacid_code('GLU', 'xyz', 1)
    with pytest.raises(ValueError, match='Invalid aminoacid code'):
        aminoacid_code('GL', 'pfm', 1)
