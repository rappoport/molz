import numpy as np
from molz import Discretizer
from molz.discretizer import ElementSphericalUniformDiscretizer, \
    SphericalUniformDiscretizer, ElementCartesianUniformDiscretizer
from pytest import approx


def test_factory_default():
    disc = Discretizer()
    assert type(disc) is ElementSphericalUniformDiscretizer


def test_factory_no_elements():
    disc = Discretizer(has_elements=False)
    assert type(disc) is SphericalUniformDiscretizer


def test_factory_cartesian_uniform():
    disc = Discretizer(coord_type='cartesian', grid_type='uniform')
    assert type(disc) is ElementCartesianUniformDiscretizer


def test_cartesian_uniform_discretize():
    disc = Discretizer(coord_type='cartesian', has_elements=False, grid_type='uniform')
    assert disc.discretize([0., 0., 0.]) == [128, 128, 128]
    assert disc.discretize([0., 0., 1.]) == [128, 128, 153]


def test_cartesian_uniform_undiscretize():
    disc = Discretizer(coord_type='cartesian', has_elements=False, grid_type='uniform')
    assert disc.undiscretize([128, 128, 128]) == approx([0.01953125, 0.01953125, 0.01953125])
    assert disc.undiscretize([128, 128, 153]) == approx([0.01953125, 0.01953125, 0.99609375])


def test_element_cartesian_uniform_discretize():
    disc = Discretizer(coord_type='cartesian', grid_type='uniform')
    assert disc.discretize([8, 0., 0., 0.]) == [8, 128, 128, 128]
    assert disc.discretize([1, 0., 0., 1.]) == [1, 128, 128, 153]


def test_element_cartesian_uniform_undiscretize():
    disc = Discretizer(coord_type='cartesian', grid_type='uniform')
    assert disc.undiscretize([8, 128, 128, 128]) == approx([8, 0.01953125, 0.01953125, 0.01953125])
    assert disc.undiscretize([1, 128, 128, 153]) == approx([1, 0.01953125, 0.01953125, 0.99609375])


def test_element_cartesian_uniform_discretize_width20():
    disc = Discretizer(coord_type='cartesian', grid_type='uniform', width=20.)
    assert disc.discretize([8, 0., 0., 0.]) == [8, 128, 128, 128]
    assert disc.discretize([1, 0., 0., 1.]) == [1, 128, 128, 140]


def test_element_cartesian_uniform_undiscretize_width20():
    disc = Discretizer(coord_type='cartesian', grid_type='uniform', width=20.)
    assert disc.undiscretize([8, 128, 128, 128]) == approx([8, 0.0390625, 0.0390625, 0.0390625])
    assert disc.undiscretize([1, 128, 128, 140]) == approx([1, 0.0390625, 0.0390625, 0.9765625])


def test_spherical_uniform_discretize():
    disc = Discretizer(coord_type='spherical', has_elements=False, grid_type='uniform')
    assert disc.discretize([0., 0., 0.]) == [0, 0, 128]
    assert disc.discretize([1., 0., -np.pi]) == [25, 0, 0]


def test_spherical_uniform_undiscretize():
    disc = Discretizer(coord_type='spherical', has_elements=False, grid_type='uniform')
    assert disc.undiscretize([0, 0, 128]) == approx([0.01953125, 0.0061359232, 0.0122718463])
    assert disc.undiscretize([25, 0, 0]) == approx([0.99609375, 0.0061359232, -3.1293208073])


def test_element_spherical_uniform_discretize():
    disc = Discretizer(coord_type='spherical', grid_type='uniform')
    assert disc.discretize([8, 0., 0., 0.]) == [8, 0, 0, 128]
    assert disc.discretize([1, 1., 0., -np.pi]) == [1, 25, 0, 0]


def test_element_spherical_uniform_undiscretize():
    disc = Discretizer(coord_type='spherical', grid_type='uniform')
    assert disc.undiscretize([8, 0, 0, 128]) == approx([8, 0.01953125, 0.0061359232, 0.0122718463])
    assert disc.undiscretize([1, 25, 0, 0]) == approx([1, 0.99609375, 0.0061359232, -3.1293208073])


def test_element_spherical_uniform_discretize_radius20():
    disc = Discretizer(coord_type='spherical', grid_type='uniform', radius=20.)
    assert disc.discretize([8, 0., 0., 0.]) == [8, 0, 0, 128]
    assert disc.discretize([1, 1., 0., -np.pi]) == [1, 12, 0, 0]


def test_element_spherical_uniform_undiscretize_radius20():
    disc = Discretizer(coord_type='spherical', grid_type='uniform', radius=20.)
    assert disc.undiscretize([8, 0, 0, 128]) == approx([8, 0.0390625, 0.0061359232, 0.0122718463])
    assert disc.undiscretize([1, 25, 0, 0]) == approx([1, 1.9921875, 0.0061359232, -3.12932081])


def test_element_spherical_uniform_discretize_16bit():
    disc = Discretizer(coord_type='spherical', grid_type='uniform', n_bits=16)
    assert disc.discretize([8, 0., 0., 0.]) == [8, 0, 0, 32768]
    assert disc.discretize([1, 1., 0., -np.pi]) == [1, 6553, 0, 0]


def test_element_spherical_uniform_undiscretize_16bit():
    disc = Discretizer(coord_type='spherical', grid_type='uniform', n_bits=16)
    assert disc.undiscretize([8, 0, 0, 32768]) == approx([8, 7.6e-05, 2.4e-05, 4.8e-05], abs=1e-6)
    assert disc.undiscretize([1, 6553, 0, 0]) == approx([1, 0.99998474, 2.4e-05, -3.14154472], abs=1e-6)
